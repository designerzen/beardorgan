# The Beard Organ #

## A wearable synthesizer that makes you look dope. ##

Controlled by the face and head and using the mouth in interesting new ways.

### Repo Structure ###

```schematic/```

***Hardware Reference*** in Fritzing and .pdf format


```source/```

***C source code*** in .ino format


```libs/```

***C source libraries*** for the daughterboard components


```tests/```

***C source code*** for ensuring that seperate, disparate code performs it's function


```experiments/```

***C source code*** for experiments and experimentation


-------------
## Lighting ##
### [NeoPixels](https://learn.adafruit.com/adafruit-neopixel-uberguide) ###

*Important Things to Know About NeoPixels in General :*

* _Before_ connecting NeoPixels to any large power source (DC “wall wart” or even a large battery), add a capacitor (1000 µF, 6.3V or higher) across the + and – terminals as shown above.

* Place a 300 to 500 Ohm resistor between the Arduino data output pin and the input to the first NeoPixel. This resistor must be at the NeoPixel end of the wire to be effective! Some products already incorporate this resistor…if you’re not sure, add one…there’s no harm in doubling up!

* Try to minimize the distance between the Arduino and first pixel.
Avoid connecting NeoPixels to a live circuit. If you simply must, always connect ground first, then +5V, then data. Disconnect in the reverse order.

* If powering the pixels with a separate supply, apply power to the pixels before applying power to the microcontroller.

* Observe the same precautions as you would for any static-sensitive part; ground yourself before handling, etc.

* NeoPixels powered by 5v require a 5V data signal. If using a 3.3V microcontroller you must use a logic level shifter such as a 74AHCT125 or 74HCT245. (If you are powering your NeoPixels with 3.7v like from a LiPoly, a 3.3v data signal is OK)

* Make sure that your connections are secure. Alligator clips do not make reliable connections to the tiny solder pads on NeoPixel rings. Better to solder a small pigtail wire to the ring and attach the alligator clips to that.

-------------


## Positioning ##
### [MPU9250](https://www.sparkfun.com/products/13762) ###

71 I should be 71
MPU9250 is online...
x-axis self test: acceleration trim within : -0.1% of factory value
y-axis self test: acceleration trim within : 0.9% of factory value
z-axis self test: acceleration trim within : 0.4% of factory value
x-axis self test: gyration trim within : -0.1% of factory value
y-axis self test: gyration trim within : -0.2% of factory value
z-axis self test: gyration trim within : 1.0% of factory value
MPU9250 initialized for active data mode....
AK8963 I AM 48 I should be 48
AK8963 initialized for active data mode....
X-Axis sensitivity adjustment value 1.20
Y-Axis sensitivity adjustment value 1.20
Z-Axis sensitivity adjustment value 1.15
ax = -87.16 ay = -1.16 az = -1002.87 mg
gx = 0.05 gy = 0.21 gz = 0.04 deg/s
mx = 0 my = -370 mz = -741 mG
q0 = 0.15 qx = -0.00 qy = -0.99 qz = 0.00
Yaw, Pitch, Roll: 171.39, -17.63, -179.94
rate = 0.55 Hz
ax = -84.11 ay = -0.61 az = -1002.20 mg
gx = 0.00 gy = 0.11 gz = 0.01 deg/s
mx = 0 my = -381 mz = -748 mG
q0 = -0.04 qx = 0.00 qy = -1.00 qz = 0.00
Yaw, Pitch, Roll: -188.36, 4.74, -179.98
rate = 219.70 Hz

-------------


## Synthesis ##
### [Mozzi](https://github.com/sensorium/Mozzi) ###

```
#include <MozziGuts.h>   // at the top of your sketch

void setup() {
    startMozzi( CONTROL_FREQUENCY );
}

void updateControl(){
    // your control code
}

int updateAudio(){
    // your audio code which returns an int between -244 and 243
}

void loop() {
    audioHook();
}
```

Caveats and Workarounds

* While Mozzi is running, the Arduino time functions millis(), micros(), delay(), and delayMicroseconds() are disabled.

* Mozzi provides EventDelay() for scheduling instead of delay, and mozziMicros() for timing, with 61us resolution (in STANDARD mode).

* Mozzi interferes with analogWrite(). In STANDARD audio mode, Mozzi takes over Timer0 (pins 5 and 6) and Timer1 (pins 9 and 10), but you can use the Timer2 pins, 3 and 11 (your board may differ). In HIFI mode, Mozzi uses Timer0, Timer1 (or Timer4 on some boards), and Timer2, so pins 3 and 11 are also out.

* If you need PWM output (analogWrite()), you can do it on any digital pins using the technique in Mozzi>examples>11.Communication>Sinewave_PWM_pins_HIFI.

* analogRead() is replaced by mozziAnalogRead(), which works in the background instead of blocking the processor.
