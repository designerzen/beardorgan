# Arduino Libraries #

## Please install these libs before attempting to compile ##

In the Arduino IDE, navigate to Sketch > Import Library.
At the top of the drop down list, select the option to Add Library.
Return to the Sketch > Import Library menu.
You should now see the library at the bottom of the drop-down menu.
It is ready to be used in your sketch.


### MPU 9250 ###
#### Position Sensor ####
(Hook Up Guide)[https://learn.sparkfun.com/tutorials/mpu-9250-hookup-guide]

### NeoPixels ###
#### Light Strip ####
(How To Guide)[https://learn.adafruit.com/adafruit-neopixel-uberguide]
