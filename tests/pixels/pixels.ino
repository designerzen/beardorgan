/* /////////////////////////////////////////////////////////////////////////////
    @Circuit: Neo Pixels on pin 6

    @License : GPL2.0

    @Developed by :
        * Robert
        * zen

    NOTE:
        Digital pin 13 is harder to use as a digital input than the other
        digital pins because it has an LED and resistor attached to it that's
        soldered to the board on most boards. If you enable its internal
        20k pull-up resistor, it will hang at around 1.7V instead of the
        expected 5V because the onboard LED and series resistor pull the
        voltage level down, meaning it always returns LOW.
        If you must use pin 13 as a digital input, set its pinMode() to
        INPUT and use an external pull down resistor.

    IMPORTANT:
        To reduce NeoPixel burnout risk, add 1000 uF capacitor across
        pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
        and minimize distance between Arduino and first pixel.

        Avoid connecting on a live circuit...if you must, connect GND first.

///////////////////////////////////////////////////////////////////////////// */



// Libraries ===================================================================
#ifdef __AVR__
  #include <avr/power.h>                // Low Power mode
#endif
#include <Adafruit_NeoPixel.h>
//#include <ADC.h>                      // Teensy 3.1 uncomment this line and install http://github.com/pedvide/ADC


// Constants ===================================================================
#define LED_PIN         6       // ARDUINO I/O setup

#define NUMPIXELS       10       // how many neopixels are on the ribbon
#define BRIGHTNESS      50      // lightness of the LEDs for the NeoPixel Outputs


// Initialisation ==============================================================

// Initialise the NeoPixels library
// Parameter 1 = number of pixels in strip,  neopixel stick has 8
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_RGB     Pixels are wired for RGB bitstream
//   NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
//   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED strip), correct for neopixel stick
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);

// Initialise ==================================================================
void setup()
{
    pixels.setBrightness(BRIGHTNESS);   // Set the output brightness
    pixels.begin();                     // This initializes the NeoPixel library.
    //pixels.show();                    // Initialize all pixels to 'off'
}

void loop()
{
    setPixels();
}

// Internal Methods ============================================================

// Control the LED pixels and beard lights!
void setPixels()
{
    // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
    // for( int i=0; i<pixels.numPixels(); ++i)
    for( uint16_t i=0; i<NUMPIXELS; ++i)
    {
        // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
        pixels.setPixelColor(i, pixels.Color( 0, 150, random(255) ));
    }
    // This sends the updated pixel color to the hardware.
    pixels.show();
}
