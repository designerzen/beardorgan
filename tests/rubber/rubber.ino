/*

Just a simple test to see what resistor is needed for the conductive rubber

*/



// START : Lighting --------------------------------------------------------------------

#include <FastLED.h>

#define PIN_ARDUINO_LED 	13      // ARDUINO I/O setup
#define PIN_LED_STRIP         	6       // NEOPIXELS I/O setup

#define PIN_GAIN_POT         	0       // Potentiometer I/O setup

#define NUM_PIXELS       	5       // how many neopixels are on the ribbon


CRGB leds[NUM_PIXELS];

int brightness 	= 255;			// lightness of the LEDs for the NeoPixel Outputs
byte angle = 0;                 // colour angle
// END : Lighting --------------------------------------------------------------------


// START : Rubber --------------------------------------------------------------------

#define FIXED_RESISTOR_IN_OHMS        1000   // Fixed reistor value in potential divider, in this case 1K
#define PIN_RUBBER_POT                1      // Potentiometer I/O setup ANALOGUE

// START : Rubber --------------------------------------------------------------------

float lastRubberReading = 0;
float lowRubberReading = 999;
float highRubberReading = 0;

void setup()
{
    Serial.begin(9600);
    FastLED.addLeds<NEOPIXEL, PIN_LED_STRIP>(leds, NUM_PIXELS);
    delay(150);
}

void loop()
{
    // Potential Divider Equation Here :
    float rubberReading = analogRead( PIN_RUBBER_POT );	// value is 0+
    if (rubberReading > 0.0 )
    {
        // between 90 and 110...
        if ( rubberReading < lowRubberReading)
        {
            lowRubberReading = rubberReading;
        }

        if ( rubberReading > highRubberReading)
        {
            highRubberReading = rubberReading;
        }


        byte mapped;
        mapped = constrain( rubberReading, 84, 180 );
        mapped = map(rubberReading, 85, 179, 0, 255);
        setPixels( mapped );

        // only values above 90 are significant really...

        Serial.print("Rubber Reading : "); Serial.print(rubberReading);
        Serial.print(" Mapped : "); Serial.println(mapped);

        // V = R / ( R + FIXED_RESISTOR_IN_OHMS ) * 5V
        // float Vout = ((5.0 / 1023.0 ) * rubberReading);
        // float b = ( 5.0 / Vout ) - 1;
        // float resistance = FIXED_RESISTOR_IN_OHMS / b;

        rubberReading = (1023 / rubberReading) -1;    // fix range 0 -> 1
        rubberReading = FIXED_RESISTOR_IN_OHMS / rubberReading;

        if ( rubberReading != lastRubberReading )
        {
            lastRubberReading = rubberReading;
            /*
            Serial.print(", Resistance : "); Serial.print(rubberReading);
            Serial.print(", Hi : "); Serial.print(highRubberReading);
            Serial.print(", Lo : "); Serial.println(lowRubberReading);*/
        }
    }

    delay(100);
}


// Control the LED pixels and beard lights!
void setPixels(byte brightness)
{
    // Clear existing values...
    FastLED.clear();
    // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
    for( int i=0; i<NUM_PIXELS; i++ )
    {
        // Hue Saturation Brightness !
        leds[i] = CHSV( angle, 187, brightness );
    }
    // This sends the updated pixel color to the hardware.
    FastLED.show();
}
