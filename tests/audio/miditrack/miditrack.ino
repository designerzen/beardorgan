
/* /////////////////////////////////////////////////////////////////////////////

As we are using 2 Arduinos,
One simply reads our data and spits out MIDI signals and flashes the lights

@Circuit:

AUDIO SYNTHESIS :
    Audio output on digital pin 9 on a Uno or similar, or DAC/A14 on Teensy 3.1,
    or check the README or http://sensorium.github.com/Mozzi/

MIDI OUT :
    Spits out a MIDI signal to 2 PORTS,
    1 of which is read by a 2nd arduiono...

    TX Pin 1 for MIDI Data (central pin) to MIDI port 5 via 220 Ohm resitors
    Ground is MIDI port 2
    5V via 220 Ohm resitors to MIDI port 4

POSITION SENSOR :
    SDA and SCL should have external pull-up resistors (to 3.3V).
    10k resistors are on the EMSENSR-9250 breakout board.

    Hardware setup:
        MPU9250 Breakout --------- Arduino
        VDD ---------------------- 3.3V
        VDDI --------------------- 3.3V
        SDA ----------------------- A4
        SCL ----------------------- A5
        GND ---------------------- GND


NEOPIXELS LIGHT STRIP :


@License : GPL2.0

@Developed by :
    * Tim Barrass 2012, CC by-nc-sa.
    * Robert
    * zen

    NOTE:
        Digital pin 13 is harder to use as a digital input than the other
        digital pins because it has an LED and resistor attached to it that's
        soldered to the board on most boards. If you enable its internal
        20k pull-up resistor, it will hang at around 1.7V instead of the
        expected 5V because the onboard LED and series resistor pull the
        voltage level down, meaning it always returns LOW.
        If you must use pin 13 as a digital input, set its pinMode() to
        INPUT and use an external pull down resistor.

    IMPORTANT:
        To reduce NeoPixel burnout risk, add 1000 uF capacitor across
        pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
        and minimize distance between Arduino and first pixel.

        Avoid connecting on a live circuit...if you must, connect GND first.

///////////////////////////////////////////////////////////////////////////// */


// Definitions ------------------------------------------------------------------------

#define AHRS                true    // Set to false for basic data read
#define SERIAL_DEBUG        true    // Set to true to get Serial output for debugging

// Pins used
#define PIN_ARDUINO_LED     13      // ARDUINO I/O setup ONBOARD LED
#define PIN_GAIN_POT   	    0       // Potentiometer I/O setup ANALOGUE Volume Control
#define PIN_NOTE_POT   	    4       // Potentiometer I/O setup ANALOGUE Note Control
#define PIN_RUBBER_POT      2       // Potentiometer I/O setup ANALOGUE Conductive Rubber

// ====================================================================================


// Libraries ==========================================================================

#ifdef __AVR__
    #include <avr/power.h>                // Low Power mode
#endif




uint16_t counter = 0;

// THROTTLE!
const uint16_t THROTTLE = 1000;




// ====================================================================================


// START : Positioning ----------------------------------------------------------------

// This fixes an issue with Arduino 1.6.4
#include <SPI.h>
#include <Wire.h>
#include <MPU9250.h>

#include <quaternionFilters.h>

// Positioning Chip 1 - head mounted
// MPU9250 headPosition;
// MPU9250 chinPosition;

// END : Positioning -----------------------------------------------------------------


// ===================================================================================


// START : Audio ---------------------------------------------------------------------

#include <MIDI.h>
#include "noteList.h"
#include "pitches.h"

#define MIDI_CHANNEL_OUTPUT 1	// MIDI Channel that gets sent out

#define PIN_SPEAKER 9       	// Speaker Output digital pin
#define PIN_MIDI_DATA 1       	// Speaker Output digital pin

byte gain;			// volume for output

// #include <SoftwareSerial.h>
// SoftwareSerial midiSerial( 2,3 );
// MIDI_CREATE_INSTANCE(SoftwareSerial, midiSerial, midiBench);
MIDI_CREATE_DEFAULT_INSTANCE();

static const unsigned sGatePin     = 13;
static const unsigned sMaxNumNotes = 16;

// cache
MidiNoteList<sMaxNumNotes> midiNotes;

// END : Audio -----------------------------------------------------------------------


// ===================================================================================



// START : Lighting ------------------------------------------------------------------
#include <FastLED.h>

#define PIN_LED_STRIP  	6       // NEOPIXELS I/O setup
#define NUM_PIXELS      5       // how many neopixels are on the ribbon

CRGB leds[NUM_PIXELS];		// Fast LED object

byte brightness = 255;		// lightness of the LEDs for the NeoPixel Outputs
byte fade = 255;
// END : Lighting --------------------------------------------------------------------

// ===================================================================================

//////////////////////////////////////////////////////////////////////////////////////
// Setup the Flashy Lights =P
//////////////////////////////////////////////////////////////////////////////////////
void setupLights()
{
    FastLED.addLeds<NEOPIXEL, PIN_LED_STRIP>(leds, NUM_PIXELS);
}

//////////////////////////////////////////////////////////////////////////////////////
// Setup the Position sensors
//////////////////////////////////////////////////////////////////////////////////////
void setupPositioning()
{
    /*
    // if we are debugging via a computer...
    byte c = headPosition.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);

    Serial.print("MPU9250 "); Serial.print("I AM "); Serial.print(c, HEX);
    Serial.print(" I should be "); Serial.println(0x71, HEX);

    // WHO_AM_I should always be 0x68
    switch (c)
    {
	case 0x71:

	    Serial.println("MPU9250 is online...");

	    // Start by performing self test and reporting values
	    headPosition.MPU9250SelfTest(headPosition.SelfTest);
	    Serial.print("x-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[0],1); Serial.println("% of factory value");
	    Serial.print("y-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[1],1); Serial.println("% of factory value");
	    Serial.print("z-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[2],1); Serial.println("% of factory value");
	    Serial.print("x-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[3],1); Serial.println("% of factory value");
	    Serial.print("y-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[4],1); Serial.println("% of factory value");
	    Serial.print("z-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[5],1); Serial.println("% of factory value");

	    // Calibrate gyro and accelerometers, load biases in bias registers
	    headPosition.calibrateMPU9250(headPosition.gyroBias, headPosition.accelBias);

	    // Initialize device for active mode read of acclerometer, gyroscope, and
	    // temperature
	    headPosition.initMPU9250();
	    Serial.println("MPU9250 initialized for active data mode....");

	    // Read the WHO_AM_I register of the magnetometer, this is a good test of
	    // communication
	    byte d = headPosition.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
	    Serial.print("AK8963 "); Serial.print("I AM "); Serial.print(d, HEX);
	    Serial.print(" I should be "); Serial.println(0x48, HEX);

	    // Get magnetometer calibration from AK8963 ROM
	    headPosition.initAK8963(headPosition.magCalibration);

	    // Initialize device for active mode read of magnetometer
	    Serial.println("AK8963 initialized for active data mode....");
	    if (SERIAL_DEBUG)
	    {
	    //  Serial.println("Calibration values: ");
		Serial.print("X-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[0], 2);
		Serial.print("Y-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[1], 2);
		Serial.print("Z-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[2], 2);
	    }

	case 0x71:
	default:

	      Serial.print("Could not connect to MPU9250: 0x");
	      Serial.println(c, HEX);
	      while(1) ; // Loop forever if communication doesn't happen

    }
    */
}

/*
// -----------------------------------------------------------------------------
void playNextNote(bool isFirstNote = false)
{
    if ( midiNotes.empty() )
    {
        // silence!
        //MIDI.sendNoteOff( note, 0, MIDI_CHANNEL_OUTPUT );
        digitalWrite(PIN_ARDUINO_LED, LOW );

    } else {

        // Possible playing modes:
        // Mono Low:  use midiNotes.getLow
        // Mono High: use midiNotes.getHigh
        // Mono Last: use midiNotes.getLast

        byte currentNote = 0;

        MidiNote midiNote = midiNotes.getLast(currentNote);

        if ( midiNote )
        {
            //some note value (note), middle velocity (0x45):
            // MIDI.sendNoteOn( sNotePitches[currentNote], gain, MIDI_CHANNEL_OUTPUT );
            MIDI.sendNoteOn( midiNote.pitch, gain, MIDI_CHANNEL_OUTPUT );   // midiNote.velocity
            digitalWrite(PIN_ARDUINO_LED, HIGH );
            //setPixels();

            midiNotes.remove( midiNote.pitch );
            //tone(sAudioOutPin, sNotePitches[currentNote]);

            if (isFirstNote)
            {

            }
            else
            {

            }
        }
    }
}
inline void handleGateChanged(bool inGateActive)
{
    digitalWrite(sGatePin, inGateActive ? HIGH : LOW);
}

inline void pulseGate()
{
// show onboard light!
    digitalWrite(PIN_ARDUINO_LED, HIGH );
    handleGateChanged(false);
    delay(1);
    handleGateChanged(true);
    digitalWrite(PIN_ARDUINO_LED, LOW );
}


void handleNotesChanged(bool isFirstNote = false)
{
    if (midiNotes.empty())
    {
        handleGateChanged(false);
        noTone(PIN_SPEAKER);
    }
    else
    {
        // Possible playing modes:
        // Mono Low:  use midiNotes.getLow
        // Mono High: use midiNotes.getHigh
        // Mono Last: use midiNotes.getLast

        byte currentNote = 0;
        if (midiNotes.getLast(currentNote))
        {
            tone(PIN_SPEAKER, sNotePitches[currentNote]);

            if (isFirstNote)
            {
                handleGateChanged(true);
            }
            else
            {
                pulseGate(); // Retrigger envelopes. Remove for legato effect.
            }
        }
    }
}

// External comms
void onNoteOn(byte inChannel, byte inNote, byte inVelocity)
{
    const bool firstNote = midiNotes.empty();
    midiNotes.add(MidiNote(inNote, inVelocity));
    handleNotesChanged(firstNote);
}

void onNoteOff(byte inChannel, byte inNote, byte inVelocity)
{
    midiNotes.remove(inNote);
    handleNotesChanged();
}
*/


//////////////////////////////////////////////////////////////////////////////////////
// Create our Audio Interface!
//////////////////////////////////////////////////////////////////////////////////////
void setupAudio()
{
    pinMode(sGatePin,     OUTPUT);
    pinMode(PIN_SPEAKER, OUTPUT);

    //MIDI.setHandleNoteOn(onNoteOn);
    //MIDI.setHandleNoteOff(onNoteOff);

    // add our song here...
    // midiNotes.add(MidiNote(inNote, inVelocity));

    // Listen to ALL channels...
    //MIDI.begin( MIDI_CHANNEL_OMNI );

    // Listen to NO channel
     MIDI.begin( MIDI_CHANNEL_OFF );
}

void playNote( byte pitch, byte velocity )
{
    MIDI.sendNoteOn( pitch, velocity, MIDI_CHANNEL_OUTPUT );   // midiNote.velocity
    digitalWrite(PIN_ARDUINO_LED, HIGH );
}

void stopNote( byte pitch )
{
    MIDI.sendNoteOff( pitch, 0, MIDI_CHANNEL_OUTPUT );
    digitalWrite(PIN_ARDUINO_LED, LOW );
}


// Initialisation ====================================================================
void setup()
{
    Serial.begin(9600); 		// for Teensy 3.1, beware printout can cause glitches
    // Serial.begin(115200);

    pinMode(PIN_ARDUINO_LED, OUTPUT );

    // Initiate the Wire library and join the I2C bus as a master or slave.
    // This should normally be called only once.
    // http://playground.arduino.cc/Main/WireLibraryDetailedReference
    //Wire.begin();

    // Lighting
    setupLights();

    // Positioning
    setupPositioning();

    // Audio MIDI Last...
    setupAudio();
}


//////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////
void loop()
{
    byte volume;
    int volumePotValue;
    boolean updateNeccessary = false;

    // play notes from F#-0 (0x1E) to F#-5 (0x5A):
    //for (int note = 0x1E; note < 0x5A; note ++)
    //{

    // rather than using the delay method, let's use the millis() command
    // to work out exactly how long since the last beat...

    // read the variable resistor for volume
    volumePotValue = analogRead( PIN_GAIN_POT );	// value is 0-1023

    // map it to an 8 bit range for efficient calculations (from 0->1023 to 0->255)
    byte noteKey = map(volumePotValue, 0, 1023, 0, 89);

    // [] 0 -> 89
    uint16_t pitch = sNotePitches[ noteKey ];

    //byte velocity = gain / 2;
    volume = map(volumePotValue, 0, 1023, 0, 127);

    // we only want it to play a sound if certain thresholds are met.
    // The first being, has the note actually changed at all?

    // save f changed
    if ( gain != volume )
    {
        // save updated values
        brightness = volume;

        // fade out the lights gradually...
        // ((int)aSin.next() * volume)>>8;

        gain = volume;

        updateNeccessary = true;
    }

    // show some lights
    setPixels();

    // play note if neccessary...
    if (updateNeccessary) playNote( pitch, volume );

    //Serial.println("MIDI:"); Serial.print(note); Serial.print(" VOL:"); Serial.print(vol);
    delay(500);

    if (updateNeccessary) stopNote( pitch );

    // clear LEDS
    clearPixels();

    // and silence...
    delay(100);


    //}
    /*

    // Watch for MIDI signals...
    if ( MIDI.read() )
    {
        // MIDI Message!
        //updateNeccessary = true;
    }


    // int notePotValue = analogRead( PIN_NOTE_POT );	// value is 0-1023
    // check for change...


    // Serial.println(volumePotValue);
    // Serial.println("POT:"); Serial.print(volumePotValue); Serial.print(" VOL:"); Serial.print(volume); Serial.print("\n");

    // check to see if it has changed and if so update other stuff...
    if ( gain != volume )
    {
	   Serial.println("Volume changed..POT:"); Serial.print(volumePotValue); Serial.print(" VOL:"); Serial.print(volume); Serial.print("\n");

        // save updated values
        brightness = volume;
        gain = volume;

        updateNeccessary = true;

    }else{

	   //Serial.println("Volume unchanged."); Serial.print(gain); Serial.print( " == " );Serial.print(volume); Serial.print("\n");
    }

    //Serial.println("updateControl gain: ");
    //Serial.print(gain);

    // put changing controls in here
    if (counter++ >= THROTTLE)
    {
        // force update
        // updateNeccessary = true;
        // reset counter
        counter %= THROTTLE;
    }

    //if(updateNeccessary || counter >= THROTTLE)
    if (updateNeccessary)
    {
        //Serial.println("-------------------------------");
	//Serial.print("updating...");

        // readPosition();

        // Send out some audio
        //byte note = map(volumePotValue, 0, 1023, 0, 128);

        //MIDI.sendNoteOn( note, 127, MIDI_CHANNEL_OUTPUT );

        //Serial.println("MIDI.sendNoteOn:"); Serial.print(note); Serial.print(" CHANNEL:"); Serial.print(MIDI_CHANNEL_OUTPUT); Serial.print("\n");

        // Control the LEDS
        //setPixels();
    }
*/
    //Serial.println("Counter:"); Serial.print( counter ); Serial.print("\n");
}


// Internal Methods ============================================================

// Control the LED pixels and beard lights!
void clearPixels()
{
    FastLED.clear();
    FastLED.show();
}

void setPixels()
{
    byte hue;
    // Clear existing values...
    FastLED.clear();
    // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
    for( int i=0; i<NUM_PIXELS; i++ )
    {
        // Hue Saturation Brightness !
        // fetch previous values ...
        hue = random(255);
        leds[i] = CHSV( hue, 187, brightness );

        // Serial.println("==================");
        // Serial.println("Painting LED : ");
        // Serial.println(i);
        // Serial.println("With RGB > ");
        // Serial.print( " R:" ); Serial.print( leds[i].red );
        // Serial.print( " G:" ); Serial.print( leds[i].green );
        // Serial.print( " B:" ); Serial.print( leds[i].blue );
        // Serial.println( " H:" ); Serial.print( hue );
        // Serial.print( " S:187" );
        // Serial.print( " V:" ); Serial.print( brightness );
        // Serial.println("==================");

        // or via RGB
        //leds[i].r = 255;
        //leds[i].g = 68;
        //leds[i].b = 221;
    }
    // This sends the updated pixel color to the hardware.
    FastLED.show();
    /*
    Serial.println("\n==================");
    Serial.print("Painting LEDS with RGB > ");
    Serial.print( " R:" ); Serial.print( leds[0].red );
    Serial.print( " G:" ); Serial.print( leds[0].green );
    Serial.print( " B:" ); Serial.print( leds[0].blue );
    Serial.print( " H:" ); Serial.print( hue );
    Serial.print( " S:187" );
    Serial.print( " V:" ); Serial.print( brightness );
    Serial.println("\n==================");
    */
}
