/* /////////////////////////////////////////////////////////////////////////////

@Circuit:

AUDIO SYNTHESIS :
    Audio output on digital pin 9 on a Uno or similar, or DAC/A14 on Teensy 3.1,
    or check the README or http://sensorium.github.com/Mozzi/

@License : GPL2.0

@Developed by :
    * Tim Barrass 2012, CC by-nc-sa.
    * Robert
    * zen

    NOTE:
        Digital pin 13 is harder to use as a digital input than the other
        digital pins because it has an LED and resistor attached to it that's
        soldered to the board on most boards. If you enable its internal
        20k pull-up resistor, it will hang at around 1.7V instead of the
        expected 5V because the onboard LED and series resistor pull the
        voltage level down, meaning it always returns LOW.
        If you must use pin 13 as a digital input, set its pinMode() to
        INPUT and use an external pull down resistor.

///////////////////////////////////////////////////////////////////////////// */


// Libraries ===================================================================

// Audio -----------------------------------------------------------------------
#include <MozziGuts.h>                  // Audio Synthesis lib
#include <Oscil.h>                      // oscillator template
// #include <EventDelay.h>                 // for scheduling events
#include <Line.h>                       // For sweeps and sequences

#include <mozzi_midi.h>                 // for mtof
#include <mozzi_fixmath.h>

// #include <tables/sin2048_int8.h>
#include <tables/sin1024_int8.h>        // sine table for oscillator at 1024 samples
// #include <tables/triangle1024_int8.h>   // triangle table for oscillator at 1024 samples
// #include <tables/brownnoise8192_int8.h>   // noise table for oscillator at 1024 samples

// #include <tables/sin256_int8.h>
// #include <tables/sin512_int8.h>
// #include <tables/sin1024_int8.h>         // sine table for oscillator at 1024 samples
// #include <tables/sin2048_int8.h>         // sine table for oscillator at 2048 samples
// #include <tables/sin4096_int8.h>
// #include <tables/sin8192_int8.h>


// Definitions ===================================================================
#define LED_PIN         6       // ARDUINO I/O setup
#define ARDUINO_LED_PIN 13      // ARDUINO I/O setup

#define SERIAL_DEBUG    true    // Set to true to get Serial output for debugging


// Variables ===================================================================
uint16_t counter = 0;

// Line to sweep frequency at control rate
Line <float> sweep;


// Constants ===================================================================
const unsigned int MILLIS_PER_SWEEP = 2000;
const unsigned int MILLIS_PER_CONTROL = 1000u / CONTROL_RATE;
const unsigned long CONTROL_STEPS_PER_SWEEP = (unsigned long) MILLIS_PER_SWEEP / MILLIS_PER_CONTROL;


// Initialisation ==============================================================

// Initialise the audio library and any oscillators
// use: Oscil <table_size, update_rate> oscilName (wavetable),
// look in .h file of table #included above

// Oscil <TRIANGLE1024_NUM_CELLS, AUDIO_RATE> aSin(TRIANGLE1024_DATA);
// Oscil <BROWNNOISE8192_NUM_CELLS, AUDIO_RATE> aSin(BROWNNOISE8192_DATA);

// Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
Oscil <SIN1024_NUM_CELLS, AUDIO_RATE> aSin(SIN1024_DATA);
// Oscil <SIN256_NUM_CELLS, AUDIO_RATE> aSin(SIN256_DATA); // can hear significant aliasing noise
// Oscil <SIN512_NUM_CELLS, AUDIO_RATE> aSin(SIN512_DATA); // noise still there but less noticeable
// Oscil <SIN1024_NUM_CELLS, AUDIO_RATE> aSin(SIN1024_DATA); // borderline, hardly there if at all
// Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA); // no audible improvement from here on
// Oscil <SIN4096_NUM_CELLS, AUDIO_RATE> aSin(SIN4096_DATA); // for 45 year old loud sound damaged ears
// Oscil <SIN8192_NUM_CELLS, AUDIO_RATE> aSin(SIN8192_DATA);

// Initialise ==================================================================
void setup()
{
    // Initiate the Wire library and join the I2C bus as a master or slave.
    // This should normally be called only once.
    // http://playground.arduino.cc/Main/WireLibraryDetailedReference
    //Wire.begin();

    // Audio
    startMozzi(CONTROL_RATE);           // set a control rate of 64 (powers of 2 please)
    sweep.set(0.f, 8192.f, CONTROL_STEPS_PER_SWEEP);
    aSin.setFreq(440);                  // set the initial frequency
}

// Mozzi Called...
// This is where we read actuator data and push buttons
void updateControl()
{
    uint16_t limit = 100;
    // put changing controls in here
    if(counter++ >= limit)
    {
        
        // reset counter
        counter %= limit;
    }
}

// Generte audio
int updateAudio()
{
    // your audio code which returns an int between -244 and 243
    return aSin.next(); // return an int signal centred around 0
}


void loop()
{
    audioHook(); // required here; fills the audio buffer
}


// Internal Methods ============================================================
