/*
 MIDI note player

 This sketch shows how to use the serial transmit pin (pin 1) to send MIDI note data.
 If this circuit is connected to a MIDI synth, it will play
 the notes F#-0 (0x1E) to F#-5 (0x5A) in sequence.

 The circuit:
 * digital in 1 connected to MIDI jack pin 5
 * MIDI jack pin 2 connected to ground
 * MIDI jack pin 4 connected to +5V through 220-ohm resistor
 Attach a MIDI cable to the jack, then to a MIDI synth, and play music.

 */

 #include <MIDI.h>

 #define MIDI_CHANNEL_OUTPUT 1	// MIDI Channel that gets sent out

 // Pins used
 #define PIN_ARDUINO_LED 13      // ARDUINO I/O setup ONBOARD LED
 // #define PIN_MIDI_DATA 1

 // #include <SoftwareSerial.h>
 // SoftwareSerial midiSerial( 2,3 );
 // MIDI_CREATE_INSTANCE(SoftwareSerial, midiSerial, midiBench);
 MIDI_CREATE_DEFAULT_INSTANCE();

 //////////////////////////////////////////////////////////////////////////////////////
 // Create our Audio Interface!
 //////////////////////////////////////////////////////////////////////////////////////
 void setupAudio()
 {
    //MIDI.setHandleNoteOn(onNoteOn);
    //MIDI.setHandleNoteOff(onNoteOff);

    // Listen to ALL channels...
    //MIDI.begin( MIDI_CHANNEL_OMNI );

    // Listen to NO channels
    MIDI.begin( MIDI_CHANNEL_OFF );
 }

 void playNote( byte pitch, byte velocity )
 {
    MIDI.sendNoteOn( pitch, velocity, MIDI_CHANNEL_OUTPUT );   // midiNote.velocity
    digitalWrite(PIN_ARDUINO_LED, HIGH );
 }

 void stopNote( byte pitch )
 {
     MIDI.sendNoteOff( pitch, 0, MIDI_CHANNEL_OUTPUT );
     digitalWrite(PIN_ARDUINO_LED, LOW );
 }

void setup()
{
    // Set MIDI baud rate:
    // Robert, you can change this to whatever you need for over USB
    Serial.begin(31250);
    setupAudio();
}

void loop()
{
    // play notes from F#-0 (0x1E) to F#-5 (0x5A):
    for (int note = 0x1E; note < 0x5A; note ++)
    {
        playNote( note, 127 );
        delay(500);

        stopNote( note );
        delay(1000);
    }
}
