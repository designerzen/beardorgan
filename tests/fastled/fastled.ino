/* /////////////////////////////////////////////////////////////////////////////

@Circuit:

@License : GPL2.0

@Developed by :
    * Tim Barrass 2012, CC by-nc-sa.
    * Robert
    * zen

    NOTE:
        Digital pin 13 is harder to use as a digital input than the other
        digital pins because it has an LED and resistor attached to it that's
        soldered to the board on most boards. If you enable its internal
        20k pull-up resistor, it will hang at around 1.7V instead of the
        expected 5V because the onboard LED and series resistor pull the
        voltage level down, meaning it always returns LOW.
        If you must use pin 13 as a digital input, set its pinMode() to
        INPUT and use an external pull down resistor.

    IMPORTANT:
        To reduce NeoPixel burnout risk, add 1000 uF capacitor across
        pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
        and minimize distance between Arduino and first pixel.

        Avoid connecting on a live circuit...if you must, connect GND first.

///////////////////////////////////////////////////////////////////////////// */


// START : Lighting --------------------------------------------------------------------

#include <FastLED.h>

#define PIN_ARDUINO_LED 	13      // ARDUINO I/O setup
#define PIN_LED_STRIP         	6       // NEOPIXELS I/O setup

#define PIN_GAIN_POT         	0       // Potentiometer I/O setup

#define NUM_PIXELS       	10       // how many neopixels are on the ribbon


CRGB leds[NUM_PIXELS];

int brightness 	= 255;			// lightness of the LEDs for the NeoPixel Outputs

// END : Lighting --------------------------------------------------------------------




// Initialise ==================================================================
void setup()
{
    Serial.begin(9600); 		// for Teensy 3.1, beware printout can cause glitches
    //Serial.begin(115200);

    FastLED.addLeds<NEOPIXEL, PIN_LED_STRIP>(leds, NUM_PIXELS);
    Serial.println("Set up LEDS");
}

//////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////
void loop()
{
    //
    int val = analogRead( PIN_GAIN_POT );
    // map the range from 0->1023 to 0->255
    //brightness = map(val, 0, 1023, 0, 255);


    setPixels();
    delay(350);
}


// Internal Methods ============================================================

// Control the LED pixels and beard lights!
void setPixels()
{
    // Clear existing values...
    FastLED.clear();
    // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
    for( int i=0; i<NUM_PIXELS; i++ )
    {
        // Hue Saturation Brightness !
	// fetch previous values ...

        leds[i] = CHSV( random(255), 255, brightness );

	Serial.println("Painting LED : ");
	Serial.print(i);
	Serial.println("With RGB : ");
	Serial.print( "R:" );
	Serial.print( leds[i].red );
	Serial.print( " G:" );
	Serial.print( leds[i].green );
	Serial.print( " B:" );
	Serial.print( leds[i].blue );

	Serial.println("With Brightness : ");
	Serial.print( brightness );


	// or via RGB
	//leds[i].r = 255;
	//leds[i].g = 68;
	//leds[i].b = 221;
    }
    // This sends the updated pixel color to the hardware.
    FastLED.show();
}
