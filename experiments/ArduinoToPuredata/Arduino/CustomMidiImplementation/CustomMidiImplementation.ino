/*
  MIDI note player

  This sketch shows how to use the serial transmit pin (pin 1) to send MIDI note data.

  The circuit:
   digital in 1 connected to MIDI jack pin 5
   MIDI jack pin 2 connected to ground
   MIDI jack pin 4 connected to +5V through 220-ohm resistor
  Attach a MIDI cable to the jack, then to a MIDI synth, and play music.

  created 13 Jun 2006
  modified 13 Aug 2012
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Midi

*/

void setup() {
  //  Set MIDI baud rate:
  Serial.begin(9600);
}

void loop() {

  midiCommandsTest();
}


void midiCommandsTest() {
  for ( int channel = 0; channel < 4; channel++ ) {
    //Test note on, note off
    for (int note = 60; note < 70; note ++) {
      sendNoteOn(channel, note, 60);
      delay(300);
      sendNoteOff(channel, note, 0);
      delay(300);
    }

    //Test aftertouch    
    for(int amount = 0; amount<20; amount++){
     sendAftertouch(channel, 60, amount);
     delay(200);
    }

    //test controller command
    for(int controller = 0; controller<20; controller++){
     sendContinuousController(channel, controller, 60);
     delay(200);
    }

    //test patch change
    for(int instrument = 0; instrument<127; instrument++){
     sendPatchChange(channel, instrument);
     delay(200);
    }

    //test channel aftertouch
    for(int amount = 0; amount<20; amount++){
     sendChannelAftertouch(channel, amount);
     delay(200);
    }

    //test pitch bend //BROKEN
//    for(int amount = 0; amount<16383; amount += 32){
//     sendPitchBend(channel, amount);
//     delay(10);
//    }
  }  
}

