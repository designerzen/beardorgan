/** 
 *  Midi commands implementation 
 *                by Robert Blaauboer
 *  ( according to https://ccrma.stanford.edu/~craig/articles/linuxmidi/misc/essenmidi.html )
 *  
 *  
 *  
 *  
 *  Extended from Midi Note Player example 
 *                by Tom Igoe
 */


/** 
 * Sends a Note Off Midi message.
 * @param channel Value between 0-16 
 * @param key Value between 0-127
 * @param velocity Value between 0-127
 */
void sendNoteOff(int channel, int key, int velocity) {
  Serial.write(0x80 | channel);
  Serial.write(key);
  Serial.write(velocity);
}


/** 
 * Sends a Note On Midi message.
 * @param channel Value between 0-16 
 * @param key Value between 0-127
 * @param velocity Value between 0-127
 */
void sendNoteOn(int channel, int key, int velocity) {
  Serial.write(0x90 | channel);
  Serial.write(key);
  Serial.write(velocity);
}


/** 
 * Sends an Aftertouch Midi message.
 * @param channel Value between 0-16 
 * @param key Value between 0-127
 * @param amount Value between 0-127
 */
void sendAftertouch(int channel, int key, int amount) {
  Serial.write(0xA0 | channel);
  Serial.write(key);
  Serial.write(amount);
}


/** 
 * Sends an Controller Midi message.
 * @param channel Value between 0-16 
 * @param controller Value between 0-127
 * @param value Value between 0-127
 */
void sendContinuousController(int channel, int controller, int value) {
  Serial.write(0xB0 | channel);
  Serial.write(controller);
  Serial.write(value);
}


/** 
 * Sends an Instrument Change Midi message.
 * @param channel Value between 0-16 
 * @param patch Value between 0-127
 */
void sendPatchChange(int channel, int patch) {
  Serial.write(0xC0 | channel);
  Serial.write(patch);
}


/** 
 * Sends a Channel Aftertouch Midi message.
 * @param channel Value between 0-16 
 * @param amount Value between 0-127
 */
void sendChannelAftertouch(int channel, int amount) {
  Serial.write(0xD0 | channel);
  Serial.write(amount);
}


/** //BROKEN
 * Sends an Aftertouch Midi message.
 * @param channel Value between 0-16 
 * @param value Value between 0-16383 (center, 8192)
 */
void sendPitchBend(int channel, int value) {
  Serial.write(0xE0 | channel);
  Serial.write(value << 9 >> 9); // 7 lsb
  Serial.write(value <<2 >> 9); // 7 msb 
}

