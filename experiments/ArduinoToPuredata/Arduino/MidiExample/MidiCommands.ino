/** 
 *  Midi commands according to https://ccrma.stanford.edu/~craig/articles/linuxmidi/misc/essenmidi.html
 *  Simplified midi spec
 */


/** 
 * Sends a Note Off Midi message.
 * @param key Value between 0-127
 * @param velocity Value between 0-127
 */
void sendNoteOff(int key, int velocity) {
  Serial.write(0x80);
  Serial.write(key);
  Serial.write(velocity);
}


/** 
 * Sends a Note On Midi message.
 * @param key Value between 0-127
 * @param velocity Value between 0-127
 */
void sendNoteOn(int key, int velocity) {
  Serial.write(0x90);
  Serial.write(key);
  Serial.write(velocity);
}


/** 
 * Sends an Aftertouch Midi message.
 * @param key Value between 0-127
 * @param amount Value between 0-127
 */
void sendAftertouch(int key, int amount) {
  Serial.write(0xA0);
  Serial.write(key);
  Serial.write(amount);
}


/** 
 * Sends an Controller Midi message.
 * @param controller Value between 0-127
 * @param value Value between 0-127
 */
void sendContinuousController(int controller, int value) {
  Serial.write(0xB0);
  Serial.write(controller);
  Serial.write(value);
}


/** 
 * Sends an Instrument Change Midi message.
 * @param patch Value between 0-127
 */
void sendPatchChange(int patch) {
  Serial.write(0xC0);
  Serial.write(instrument);
}


/** 
 * Sends a Channel Aftertouch Midi message.
 * @param amount Value between 0-127
 */
void sendChannelAftertouch(int amount) {
  Serial.write(0xD0);
  Serial.write(amount);
}


/** 
 * Sends an Aftertouch Midi message.
 * @param value Value between 0-16383 (center, 8192)
 */
void sendPitchBend(int value) {
  Serial.write(0xE0);
  Serial.write(value);
}

