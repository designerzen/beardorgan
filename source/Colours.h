#ifndef COLOURS_H_
#define COLOURS_H_

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t colourise( byte value )
{
    value = 255 - value;
    if(value < 85)
    {
        return strip.Color(255 - value * 3, 0, value * 3,0);
    }
    if(value < 170)
    {
        value -= 85;
        return strip.Color(0, value * 3, 255 - value * 3,0);
    }
    value -= 170;
    return strip.Color(value * 3, 255 - value * 3, 0,0);
}

// 0->255 -> HEX
uint8_t red(uint32_t c)
{
    return (c >> 8);
}

uint8_t green(uint32_t c)
{
    return (c >> 16);
}

uint8_t blue(uint32_t c)
{
    return (c);
}