#ifndef AUDIO_H
#define AUDIO_H

#include <Arduino.h>

class Audio
{
    public:

	// construct
	Audio();

	// destruct
	~Audio();

	// public methods
	void start();		// Initialise
	void play(int note);	// Play a note
	void stop();		// Cease all audio
	void intro(int time);	// Play a jingle :D
	void audioHook();
	int updateAudio();

    private:
	void updateControl();


};

#endif
