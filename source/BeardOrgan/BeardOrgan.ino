/* /////////////////////////////////////////////////////////////////////////////

@Circuit:

AUDIO SYNTHESIS :
    Audio output on digital pin 9 on a Uno or similar, or DAC/A14 on Teensy 3.1,
    or check the README or http://sensorium.github.com/Mozzi/

MIDI OUT :
    Spits out a MIDI signal to 2 PORTS,
    1 of which is read by a 2nd arduiono...

POSITION SENSOR :
    SDA and SCL should have external pull-up resistors (to 3.3V).
    10k resistors are on the EMSENSR-9250 breakout board.

    Hardware setup:
        MPU9250 Breakout --------- Arduino
        VDD ---------------------- 3.3V
        VDDI --------------------- 3.3V
        SDA ----------------------- A4
        SCL ----------------------- A5
        GND ---------------------- GND


NEOPIXELS LIGHT STRIP :



@License : GPL2.0

@Developed by :
    * Tim Barrass 2012, CC by-nc-sa.
    * Robert
    * zen

    NOTE:
        Digital pin 13 is harder to use as a digital input than the other
        digital pins because it has an LED and resistor attached to it that's
        soldered to the board on most boards. If you enable its internal
        20k pull-up resistor, it will hang at around 1.7V instead of the
        expected 5V because the onboard LED and series resistor pull the
        voltage level down, meaning it always returns LOW.
        If you must use pin 13 as a digital input, set its pinMode() to
        INPUT and use an external pull down resistor.

    IMPORTANT:
        To reduce NeoPixel burnout risk, add 1000 uF capacitor across
        pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
        and minimize distance between Arduino and first pixel.

        Avoid connecting on a live circuit...if you must, connect GND first.

///////////////////////////////////////////////////////////////////////////// */


// Definitions ========================================================================

#define AHRS            true    // Set to false for basic data read
#define SERIAL_DEBUG    true    // Set to true to get Serial output for debugging

// Pins used
#define PIN_ARDUINO_LED 13      // ARDUINO I/O setup ONBOARD LED
#define PIN_GAIN_POT   	0       // Potentiometer I/O setup ANALOGUE

// Libraries ==========================================================================

// Lighting ---------------------------------------------------------------------------
#ifdef __AVR__
    #include <avr/power.h>                // Low Power mode
#endif


// START : Positioning ----------------------------------------------------------------

// This fixes an issue with Arduino 1.6.4
//#include <SPI.h>
//#include <Wire.h>
//#include <MPU9250.h>

// #include <quaternionFilters.h>

// Positioning Chip 1 - head mounted
// MPU9250 headPosition;
// MPU9250 chinPosition;

// END : Positioning -------------------------------------------------------------------



// START : Audio -----------------------------------------------------------------------

// #include "Audio.h"			// Local lib!
// Audio audio;

#define CONTROL_RATE    64      	// powers of 2 please


#include <MozziGuts.h>                  // Audio Synthesis lib
#include <Oscil.h>                      // oscillator template
//#include <EventDelay.h>                 // for scheduling events
//#include <Line.h>                       // For sweeps and sequences
//#include <RollingAverage.h>		// Engage smoother rolls from pots

//#include <mozzi_midi.h>                 // for mtof
//#include <mozzi_fixmath.h>

#include <tables/sin2048_int8.h>
// #include <tables/sin1024_int8.h>        	// sine table for oscillator at 1024 samples
// #include <tables/triangle1024_int8.h>   	// triangle table for oscillator at 1024 samples
// #include <tables/brownnoise8192_int8.h>   	// noise table for oscillator at 1024 samples

// #include <tables/sin256_int8.h>
// #include <tables/sin512_int8.h>
// #include <tables/sin1024_int8.h>         	// sine table for oscillator at 1024 samples
// #include <tables/sin2048_int8.h>         	// sine table for oscillator at 2048 samples
// #include <tables/sin4096_int8.h>
// #include <tables/sin8192_int8.h>

// Initialise the audio library and any oscillators
// use: Oscil <table_size, update_rate> oscilName (wavetable),
// look in .h file of table #included above

// Oscil <TRIANGLE1024_NUM_CELLS, AUDIO_RATE> aSin(TRIANGLE1024_DATA);
// Oscil <BROWNNOISE8192_NUM_CELLS, AUDIO_RATE> aSin(BROWNNOISE8192_DATA);

Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
// Oscil <SIN1024_NUM_CELLS, AUDIO_RATE> aSin(SIN1024_DATA);
// Oscil <SIN256_NUM_CELLS, AUDIO_RATE> aSin(SIN256_DATA); // can hear significant aliasing noise
// Oscil <SIN512_NUM_CELLS, AUDIO_RATE> aSin(SIN512_DATA); // noise still there but less noticeable
// Oscil <SIN1024_NUM_CELLS, AUDIO_RATE> aSin(SIN1024_DATA); // borderline, hardly there if at all
// Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA); // no audible improvement from here on
// Oscil <SIN4096_NUM_CELLS, AUDIO_RATE> aSin(SIN4096_DATA); // for 45 year old loud sound damaged ears
// Oscil <SIN8192_NUM_CELLS, AUDIO_RATE> aSin(SIN8192_DATA);

// Control Volume
byte gain = 255;	// 0->255
/*
// Line to sweep frequency at control rate
Line <float> sweep;

// use: RollingAverage <number_type, how_many_to_average> myThing
RollingAverage <int, 32> kAverage; // how_many_to_average has to be power of 2
const unsigned int MILLIS_PER_SWEEP = 2000;
const unsigned int MILLIS_PER_CONTROL = 1000u / CONTROL_RATE;
const unsigned long CONTROL_STEPS_PER_SWEEP = (unsigned long) MILLIS_PER_SWEEP / MILLIS_PER_CONTROL;
*/

// END : Audio --------------------------------------------------------------------




// Variables ===================================================================
uint16_t counter = 0;
// THROTTLE!
const uint16_t THROTTLE = 10;


// Initialisation ==============================================================






// START : Lighting --------------------------------------------------------------------
//#include <FastLED.h>


#define PIN_LED_STRIP  	6       // NEOPIXELS I/O setup

#define NUM_PIXELS      3       // how many neopixels are on the ribbon

//CRGB leds[NUM_PIXELS];		// Fast LED object

//byte brightness = 255;		// lightness of the LEDs for the NeoPixel Outputs

// END : Lighting --------------------------------------------------------------------



// Initialise ==================================================================
void setup()
{
    //Serial.begin(9600); 		// for Teensy 3.1, beware printout can cause glitches
    Serial.begin(115200);

    // Initiate the Wire library and join the I2C bus as a master or slave.
    // This should normally be called only once.
    // http://playground.arduino.cc/Main/WireLibraryDetailedReference
    //Wire.begin();

    // Lighting
   // FastLED.addLeds<NEOPIXEL, PIN_LED_STRIP>(leds, NUM_PIXELS);

    // Positioning
    /*
    // if we are debugging via a computer...
    byte c = headPosition.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);

    Serial.print("MPU9250 "); Serial.print("I AM "); Serial.print(c, HEX);
    Serial.print(" I should be "); Serial.println(0x71, HEX);

    // WHO_AM_I should always be 0x68
    switch (c)
    {
	case 0x71:

	    Serial.println("MPU9250 is online...");

	    // Start by performing self test and reporting values
	    headPosition.MPU9250SelfTest(headPosition.SelfTest);
	    Serial.print("x-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[0],1); Serial.println("% of factory value");
	    Serial.print("y-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[1],1); Serial.println("% of factory value");
	    Serial.print("z-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[2],1); Serial.println("% of factory value");
	    Serial.print("x-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[3],1); Serial.println("% of factory value");
	    Serial.print("y-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[4],1); Serial.println("% of factory value");
	    Serial.print("z-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[5],1); Serial.println("% of factory value");

	    // Calibrate gyro and accelerometers, load biases in bias registers
	    headPosition.calibrateMPU9250(headPosition.gyroBias, headPosition.accelBias);

	    // Initialize device for active mode read of acclerometer, gyroscope, and
	    // temperature
	    headPosition.initMPU9250();
	    Serial.println("MPU9250 initialized for active data mode....");

	    // Read the WHO_AM_I register of the magnetometer, this is a good test of
	    // communication
	    byte d = headPosition.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
	    Serial.print("AK8963 "); Serial.print("I AM "); Serial.print(d, HEX);
	    Serial.print(" I should be "); Serial.println(0x48, HEX);

	    // Get magnetometer calibration from AK8963 ROM
	    headPosition.initAK8963(headPosition.magCalibration);

	    // Initialize device for active mode read of magnetometer
	    Serial.println("AK8963 initialized for active data mode....");
	    if (SERIAL_DEBUG)
	    {
	    //  Serial.println("Calibration values: ");
		Serial.print("X-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[0], 2);
		Serial.print("Y-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[1], 2);
		Serial.print("Z-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[2], 2);
	    }

	case 0x71:
	default:

	      Serial.print("Could not connect to MPU9250: 0x");
	      Serial.println(c, HEX);
	      while(1) ; // Loop forever if communication doesn't happen

    }
    */

    // Audio Last...
    //sweep.set(0.f, 8192.f, CONTROL_STEPS_PER_SWEEP);
    aSin.setFreq(440);                  // set the initial frequency
    startMozzi(CONTROL_RATE);           // set a control rate of 64 (powers of 2 please)

    // wait before starting Mozzi to receive analog reads, so AutoRange will not get 0
    // delay(200);

    //audio.start();
}


//////////////////////////////////////////////////////////////////////////////////
// Mozzi Called...
// This is where we read actuator data and push buttons
//////////////////////////////////////////////////////////////////////////////////
void updateControl()
{
    boolean updateNeccessary = false;

    // NB. Using non-blocking mozziAnalogRead();
    // read the variable resistor for volume
    // int val = analogRead( PIN_GAIN_POT );
    int volumePotValue = mozziAnalogRead( PIN_GAIN_POT ); // value is 0-1023

    // map it to an 8 bit range for efficient calculations in updateAudio
    // map the range from 0->1023 to 0->255
    //gain = map(volumePotValue, 0, 1023, 0, 255);

    // check to see if it has changed and if so update other stuff...
    byte volume = map(volumePotValue, 0, 1023, 0, 255);

    //Serial.println(volumePotValue);
    Serial.println("POT:"); Serial.print(volumePotValue); Serial.print(" VOL:"); Serial.print(volume); Serial.print("\n");

    /*

    //Serial.println("\n");
    if ( gain != volume )
    {
	Serial.println("Volume changed..POT:"); Serial.print(volumePotValue); Serial.print(" VOL:"); Serial.print(volume); Serial.print("\n");

	// save updated values
	brightness = volume;
	gain = volume;

	updateNeccessary = true;

    }else{

	//Serial.println("Volume unchanged."); Serial.print(gain); Serial.print( " == " );Serial.print(volume); Serial.print("\n");
    }
    */
    //int averaged = kAverage.next(volume);
    //Serial.println("updateControl gain: ");
    //Serial.print(gain);

    
    // put changing controls in here
    if (counter++ >= THROTTLE)
    {
	// force update
	updateNeccessary = true;
	// reset counter
	counter %= THROTTLE;
    }

    //if(updateNeccessary || counter >= THROTTLE)
    if (updateNeccessary)
    {
        //Serial.println("-------------------------------");
	//Serial.print("updating...");

        //setPixels();
        // readPosition();
    }

    //Serial.println("Counter:"); Serial.print( counter ); Serial.print("\n");
}

//////////////////////////////////////////////////////////////////////////////////
// Generate audio
//////////////////////////////////////////////////////////////////////////////////
int updateAudio()
{
    // your audio code which returns an int between -244 and 243
    return 0;
    // return ((int)aSin.next() * gain)>>8; // shift back into range after multiplying by 8 bit value
    //aSin.next() * gain; // return an int signal centred around 0
}


//////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////
void loop()
{
    //audio.audioHook();  		// required here; fills the audio buffer
    audioHook(); 			// required here; fills the audio buffer
}


// Internal Methods ============================================================
/*
// Control the LED pixels and beard lights!
void setPixels()
{
    byte hue;
    // Clear existing values...
    FastLED.clear();
    // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
    for( int i=0; i<NUM_PIXELS; i++ )
    {
        // Hue Saturation Brightness !
	// fetch previous values ...
	hue = random(255);
        leds[i] = CHSV( hue, 187, brightness );

	// Serial.println("==================");
	// Serial.println("Painting LED : ");
	// Serial.println(i);
	// Serial.println("With RGB > ");
	// Serial.print( " R:" ); Serial.print( leds[i].red );
	// Serial.print( " G:" ); Serial.print( leds[i].green );
	// Serial.print( " B:" ); Serial.print( leds[i].blue );
	// Serial.println( " H:" ); Serial.print( hue );
	// Serial.print( " S:187" );
	// Serial.print( " V:" ); Serial.print( brightness );
	// Serial.println("==================");

	// or via RGB
	//leds[i].r = 255;
	//leds[i].g = 68;
	//leds[i].b = 221;
    }
    // This sends the updated pixel color to the hardware.
    FastLED.show();

    Serial.println("\n==================");
    Serial.print("Painting LEDS with RGB > ");
    Serial.print( " R:" ); Serial.print( leds[0].red );
    Serial.print( " G:" ); Serial.print( leds[0].green );
    Serial.print( " B:" ); Serial.print( leds[0].blue );
    Serial.print( " H:" ); Serial.print( hue );
    Serial.print( " S:187" );
    Serial.print( " V:" ); Serial.print( brightness );
    Serial.println("\n==================");
}
*/

// As we are using Mozzi Library, we are forced to use
// Mozzi specific versions of millis() called
// Note : There are 1,000 microseconds in a millisecond
// and 1,000,000 microseconds in a second
// therefore 1 millis() / 1000 == microsec()
// or 1 millis() == microsec() * 1000

// Read the positions
void readPosition()
{
    /*
    // If intPin goes high, all data registers have new data
    // On interrupt, check if data ready interrupt
    if (headPosition.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)
    {
        headPosition.readAccelData(headPosition.accelCount);  // Read the x/y/z adc values
        headPosition.getAres();

        // Now we'll calculate the accleration value into actual g's
        // This depends on scale being set
        headPosition.ax = (float)headPosition.accelCount[0]*headPosition.aRes; // - accelBias[0];
        headPosition.ay = (float)headPosition.accelCount[1]*headPosition.aRes; // - accelBias[1];
        headPosition.az = (float)headPosition.accelCount[2]*headPosition.aRes; // - accelBias[2];

        headPosition.readGyroData(headPosition.gyroCount);  // Read the x/y/z adc values
        headPosition.getGres();

        // Calculate the gyro value into actual degrees per second
        // This depends on scale being set
        headPosition.gx = (float)headPosition.gyroCount[0]*headPosition.gRes;
        headPosition.gy = (float)headPosition.gyroCount[1]*headPosition.gRes;
        headPosition.gz = (float)headPosition.gyroCount[2]*headPosition.gRes;

        headPosition.readMagData(headPosition.magCount);  // Read the x/y/z adc values
        headPosition.getMres();
        // User environmental x-axis correction in milliGauss, should be
        // automatically calculated
        headPosition.magbias[0] = +470.;
        // User environmental x-axis correction in milliGauss TODO axis??
        headPosition.magbias[1] = +120.;
        // User environmental x-axis correction in milliGauss
        headPosition.magbias[2] = +125.;

        // Calculate the magnetometer values in milliGauss
        // Include factory calibration per data sheet and user environmental
        // corrections
        // Get actual magnetometer value, this depends on scale being set
        headPosition.mx = (float)headPosition.magCount[0]*headPosition.mRes*headPosition.magCalibration[0] -
                   headPosition.magbias[0];
        headPosition.my = (float)headPosition.magCount[1]*headPosition.mRes*headPosition.magCalibration[1] -
                   headPosition.magbias[1];
        headPosition.mz = (float)headPosition.magCount[2]*headPosition.mRes*headPosition.magCalibration[2] -
                   headPosition.magbias[2];
    } // if (readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)

    // Must be called before updating quaternions!
    headPosition.updateTime();
*/
    /*
    // Sensors x (y)-axis of the accelerometer is aligned with the y (x)-axis of
    // the magnetometer; the magnetometer z-axis (+ down) is opposite to z-axis
    // (+ up) of accelerometer and gyro! We have to make some allowance for this
    // orientationmismatch in feeding the output to the quaternion filter. For the
    // MPU-9250, we have chosen a magnetic rotation that keeps the sensor forward
    // along the x-axis just like in the LSM9DS0 sensor. This rotation can be
    // modified to allow any convenient orientation convention. This is ok by
    // aircraft orientation standards! Pass gyro rate as rad/s
    //  MadgwickQuaternionUpdate(ax, ay, az, gx*PI/180.0f, gy*PI/180.0f, gz*PI/180.0f,  my,  mx, mz);
    MahonyQuaternionUpdate(headPosition.ax, headPosition.ay, headPosition.az, headPosition.gx*DEG_TO_RAD,
                         headPosition.gy*DEG_TO_RAD, headPosition.gz*DEG_TO_RAD, headPosition.my,
                         headPosition.mx, headPosition.mz, headPosition.deltat);

    
    */
    /*

    if (!AHRS)
    {
        //headPosition.delt_t = millis() - headPosition.count;
        if (headPosition.delt_t > 500)
        {
            if(SERIAL_DEBUG)
            {
                // Print acceleration values in milligs!
                Serial.print("X-acceleration: "); Serial.print(1000*headPosition.ax);
                Serial.print(" mg ");
                Serial.print("Y-acceleration: "); Serial.print(1000*headPosition.ay);
                Serial.print(" mg ");
                Serial.print("Z-acceleration: "); Serial.print(1000*headPosition.az);
                Serial.println(" mg ");

                // Print gyro values in degree/sec
                Serial.print("X-gyro rate: "); Serial.print(headPosition.gx, 3);
                Serial.print(" degrees/sec ");
                Serial.print("Y-gyro rate: "); Serial.print(headPosition.gy, 3);
                Serial.print(" degrees/sec ");
                Serial.print("Z-gyro rate: "); Serial.print(headPosition.gz, 3);
                Serial.println(" degrees/sec");

                // Print mag values in degree/sec
                Serial.print("X-mag field: "); Serial.print(headPosition.mx);
                Serial.print(" mG ");
                Serial.print("Y-mag field: "); Serial.print(headPosition.my);
                Serial.print(" mG ");
                Serial.print("Z-mag field: "); Serial.print(headPosition.mz);
                Serial.println(" mG");

                headPosition.tempCount = headPosition.readTempData();  // Read the adc values
                // Temperature in degrees Centigrade
                headPosition.temperature = ((float) headPosition.tempCount) / 333.87 + 21.0;
                // Print temperature in degrees Centigrade
                Serial.print("Temperature is ");  Serial.print(headPosition.temperature, 1);
                Serial.println(" degrees C");
            }
            //headPosition.count = millis();
            
            // toggle led
            digitalWrite(ARDUINO_LED_PIN, !digitalRead(ARDUINO_LED_PIN));  
        } // if (headPosition.delt_t > 500)
    } // if (!AHRS)
    else
    {
        // Serial print and/or display at 0.5 s rate independent of data rates
        //headPosition.delt_t = millis() - headPosition.count;

        // update LCD once per half-second independent of read rate
        if (headPosition.delt_t > 500)
        {
            if(SERIAL_DEBUG)
            {
                Serial.print("ax = "); Serial.print((int)1000*headPosition.ax);
                Serial.print(" ay = "); Serial.print((int)1000*headPosition.ay);
                Serial.print(" az = "); Serial.print((int)1000*headPosition.az);
                Serial.println(" mg");

                Serial.print("gx = "); Serial.print( headPosition.gx, 2);
                Serial.print(" gy = "); Serial.print( headPosition.gy, 2);
                Serial.print(" gz = "); Serial.print( headPosition.gz, 2);
                Serial.println(" deg/s");

                Serial.print("mx = "); Serial.print( (int)headPosition.mx );
                Serial.print(" my = "); Serial.print( (int)headPosition.my );
                Serial.print(" mz = "); Serial.print( (int)headPosition.mz );
                Serial.println(" mG");

		// This relies on Quarternions.h

                // Serial.print("q0 = "); Serial.print(*getQ());
                // Serial.print(" qx = "); Serial.print(*(getQ() + 1));
                // Serial.print(" qy = "); Serial.print(*(getQ() + 2));
                // Serial.print(" qz = "); Serial.println(*(getQ() + 3));
            }

            // Define output variables from updated quaternion---these are Tait-Bryan
            // angles, commonly used in aircraft orientation. In this coordinate system,
            // the positive z-axis is down toward Earth. Yaw is the angle between Sensor
            // x-axis and Earth magnetic North (or true North if corrected for local
            // declination, looking down on the sensor positive yaw is counterclockwise.
            // Pitch is angle between sensor x-axis and Earth ground plane, toward the
            // Earth is positive, up toward the sky is negative. Roll is angle between
            // sensor y-axis and Earth ground plane, y-axis up is positive roll. These
            // arise from the definition of the homogeneous rotation matrix constructed
            // from quaternions. Tait-Bryan angles as well as Euler angles are
            // non-commutative; that is, the get the correct orientation the rotations
            // must be applied in the correct order which for this configuration is yaw,
            // pitch, and then roll.
            // For more see
            // http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
            // which has additional links.
            
            // Needs Quaternions.h
            headPosition.yaw   = atan2(2.0f * (*(getQ()+1) * *(getQ()+2) + *getQ() *
            *(getQ()+3)), *getQ() * *getQ() + *(getQ()+1) * *(getQ()+1)
            - *(getQ()+2) * *(getQ()+2) - *(getQ()+3) * *(getQ()+3));

            headPosition.pitch = -asin(2.0f * (*(getQ()+1) * *(getQ()+3) - *getQ() *
            *(getQ()+2)));

            headPosition.roll  = atan2(2.0f * (*getQ() * *(getQ()+1) + *(getQ()+2) *
            *(getQ()+3)), *getQ() * *getQ() - *(getQ()+1) * *(getQ()+1)
            - *(getQ()+2) * *(getQ()+2) + *(getQ()+3) * *(getQ()+3));

            

            headPosition.pitch *= RAD_TO_DEG;
            headPosition.yaw   *= RAD_TO_DEG;

            // Declination of SparkFun Electronics (40°05'26.6"N 105°11'05.9"W) is
            // 	8° 30' E  ± 0° 21' (or 8.5°) on 2016-07-19
            // - http://www.ngdc.noaa.gov/geomag-web/#declination
            headPosition.yaw   -= 8.5;
            headPosition.roll  *= RAD_TO_DEG;

            if(SERIAL_DEBUG)
            {
                Serial.print("Yaw, Pitch, Roll: ");
                Serial.print(headPosition.yaw, 2);
                Serial.print(", ");
                Serial.print(headPosition.pitch, 2);
                Serial.print(", ");
                Serial.println(headPosition.roll, 2);

                Serial.print("rate = ");
                Serial.print((float)headPosition.sumCount/headPosition.sum, 2);
                Serial.println(" Hz");
            }

            //headPosition.count = millis();
            headPosition.sumCount = 0;
            headPosition.sum = 0;
        } // if (headPosition.delt_t > 500)
    } // if (AHRS)

    */

}
