
#include "Audio.h" //include the declaration for this class

// Load in our external library...
// This class simply handles the augmentation of how audio is handled
// If we want midi output, add it here, just follow the interface set in the Audio.h file


/* */

//////////////////////////////////////////////////////////////////////////
//
// START OF MOZZI =========================================
// MOZZI!
//
//////////////////////////////////////////////////////////////////////////

#include <MozziGuts.h>                  // Audio Synthesis lib
#include <Oscil.h>                      // oscillator template
// #include <EventDelay.h>                 // for scheduling events
#include <Line.h>                       // For sweeps and sequences
#include <RollingAverage.h>		// Engage smoother rolls from pots

#include <mozzi_midi.h>                 // for mtof
#include <mozzi_fixmath.h>

// #include <tables/sin2048_int8.h>
#include <tables/sin1024_int8.h>        // sine table for oscillator at 1024 samples
// #include <tables/triangle1024_int8.h>   // triangle table for oscillator at 1024 samples
// #include <tables/brownnoise8192_int8.h>   // noise table for oscillator at 1024 samples

// #include <tables/sin256_int8.h>
// #include <tables/sin512_int8.h>
// #include <tables/sin1024_int8.h>         // sine table for oscillator at 1024 samples
// #include <tables/sin2048_int8.h>         // sine table for oscillator at 2048 samples
// #include <tables/sin4096_int8.h>
// #include <tables/sin8192_int8.h>


// Control Volume
byte gain;

// Line to sweep frequency at control rate
Line <float> sweep;

// use: RollingAverage <number_type, how_many_to_average> myThing
RollingAverage <int, 32> kAverage; // how_many_to_average has to be power of 2

// Constants ===================================================================
const unsigned int MILLIS_PER_SWEEP = 2000;
const unsigned int MILLIS_PER_CONTROL = 1000u / CONTROL_RATE;
const unsigned long CONTROL_STEPS_PER_SWEEP = (unsigned long) MILLIS_PER_SWEEP / MILLIS_PER_CONTROL;

#define CONTROL_RATE 64      // powers of 2 please

#define POTENTIOMETER_GAIN_PIN 0 // ARDUINO I/O Setup for Volume dial

Oscil <SIN1024_NUM_CELLS, AUDIO_RATE> aSin(SIN1024_DATA);

// <<constructor>> setup
Audio::Audio()
{

}

// <<destructor>>
Audio::~Audio()
{
    // nothing to destruct

}

void Audio::start()
{
    sweep.set(0.f, 8192.f, CONTROL_STEPS_PER_SWEEP);
    aSin.setFreq(440);                  // set the initial frequency
    startMozzi(CONTROL_RATE);           // set a control rate of 64 (powers of 2 please)
}

// Play Note
void Audio::play(int note)
{

}

// Stop all sound
void Audio::stop()
{

}

// Play some intro sounds to prove things are working
void Audio::intro(int time)
{


}



//////////////////////////////////////////////////////////////////////////////////
// Mozzi Called...
// This is where we read actuator data and push buttons
//////////////////////////////////////////////////////////////////////////////////
void Audio::updateControl()
{
    // Using non-blocking
    // mozziAnalogRead();
    // read the variable resistor for volume
    int sensor_value = mozziAnalogRead(POTENTIOMETER_GAIN_PIN); // value is 0-1023

    // map it to an 8 bit range for efficient calculations in updateAudio
    //gain = map(sensor_value, 0, 1023, 0, 255);

    // check to see if it has changed and if so update other stuff...
    byte volume = map(sensor_value, 0, 1023, 0, 255);
    if ( gain != volume )
    {

	gain = volume;
    }

    int averaged = kAverage.next(volume);


    uint16_t limit = 100;

}

//////////////////////////////////////////////////////////////////////////////////
// Generate audio
//////////////////////////////////////////////////////////////////////////////////
int Audio::updateAudio()
{
    // your audio code which returns an int between -244 and 243
    return ((int)aSin.next() * gain)>>8; // shift back into range after multiplying by 8 bit value
    //aSin.next() * gain; // return an int signal centred around 0
}

void Audio::audiohook()
{
  audiohook();
}

// END OF MOZZI =========================================
