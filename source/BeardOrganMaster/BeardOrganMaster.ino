
/* /////////////////////////////////////////////////////////////////////////////

As we are using 2 Arduinos,
One simply reads our data and spits out MIDI signals and flashes the lights

@Circuit:

AUDIO SYNTHESIS :
    Audio output on digital pin 9 on a Uno or similar, or DAC/A14 on Teensy 3.1,
    or check the README or http://sensorium.github.com/Mozzi/

MIDI OUT :
    Spits out a MIDI signal to 2 PORTS,
    1 of which is read by a 2nd arduiono...

    TX Pin 1 for MIDI Data (central pin) to MIDI port 5 via 220 Ohm resitors
    Ground is MIDI port 2
    5V via 220 Ohm resitors to MIDI port 4

POSITION SENSOR :
    SDA and SCL should have external pull-up resistors (to 3.3V).
    10k resistors are on the EMSENSR-9250 breakout board.

    Hardware setup:
        MPU9250 Breakout --------- Arduino
        VDD ---------------------- 3.3V
        VDDI --------------------- 3.3V
        SDA ----------------------- A4
        SCL ----------------------- A5
        GND ---------------------- GND


NEOPIXELS LIGHT STRIP :


@License : GPL2.0

@Developed by :
    * Tim Barrass 2012, CC by-nc-sa.
    * Robert
    * zen

    NOTE:
        Digital pin 13 is harder to use as a digital input than the other
        digital pins because it has an LED and resistor attached to it that's
        soldered to the board on most boards. If you enable its internal
        20k pull-up resistor, it will hang at around 1.7V instead of the
        expected 5V because the onboard LED and series resistor pull the
        voltage level down, meaning it always returns LOW.
        If you must use pin 13 as a digital input, set its pinMode() to
        INPUT and use an external pull down resistor.

    IMPORTANT:
        To reduce NeoPixel burnout risk, add 1000 uF capacitor across
        pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
        and minimize distance between Arduino and first pixel.

        Avoid connecting on a live circuit...if you must, connect GND first.

///////////////////////////////////////////////////////////////////////////// */


// Definitions ------------------------------------------------------------------------

#define AHRS            	true    	// Set to false for basic data read NB. much faster when false
#define SERIAL_DEBUG    	false    	// Set to true to get Serial output for debugging
#define USE_POSITIONING 	false    	// Use the position sensor
#define USE_CAPACTIVE_TOUCH true    	// Enable touch based switches

// Pins used ===========================================================================

// Analogue pins :
#define PIN_GAIN_POT	    	0      	// Potentiometer I/O ANALOGUE Volume Control
#define PIN_RUBBER_POT      	1      	// Potentiometer I/O ANALOGUE Conductive Rubber
#define PIN_CAPACITIVE_TOP  	2      	// Capactitance copper section on top
#define PIN_CAPACITIVE_BOTTOM 	3      	// Capactitance copper section on bottom
#define PIN_POSITION_SDA    	4      	// Position Sensor I/O Serial Data
#define PIN_POSITION_SCL    	5	// Position Sensor I/O Serial clock

// Digital pins :
//#define PIN_MIDI_DATA     	1      	// MIDI on Tx port
//#define PIN		  	2      	// UNUSED
//#define PIN		  	3      	// UNUSED
//#define PIN		  	4      	// UNUSED
#define PIN_LED_STRIP_A  	5      	// NEOPIXELS I/O setup
#define PIN_LED_STRIP_B  	6      	// NEOPIXELS I/O setup
//#define PIN		  	7      	// UNUSED
#define PIN_GATE_TRIGGER    	8	// * CV / Gate pulse output (to connect to synch output)
#define PIN_SPEAKER 	    	9      	// * Speaker Output digital pin for use with Tone()

#define PIN_ARDUINO_LED     13     // ARDUINO I/O setup ONBOARD LED

// * Unused in production - just for development...

// ====================================================================================


// Libraries ==========================================================================

#ifdef __AVR__
    #include <avr/power.h>                // Low Power mode
#endif


// THROTTLE!
uint16_t counter = 0;
const uint16_t THROTTLE = 1000;
int noteDuration = 100; 


///////////////////////////////////////////////////////////////////////////////////////
// START : Conductive Rubber ----------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////

float lastRubberReading = 0.0;

const byte lowerLimit = 170;	// 170
const int upperLimit = 268;	// 268

// how many samples to take and average, more takes longer but is more 'smooth'
#define NUMSAMPLES 5
//int samples[NUMSAMPLES];

///////////////////////////////////////////////////////////////////////////////////////
// END : Conductive Rubber ------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
// START : Capactitance Bands ---------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////

#include <ADCTouch.h>
int capacitanceBaselineTop, capacitanceBaselineBottom;

///////////////////////////////////////////////////////////////////////////////////////
// END : Capactitance Bands -----------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////
// START : Positioning ----------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////

// This fixes an issue with Arduino 1.6.4
#include <SPI.h>
#include <Wire.h>
#include <MPU9250.h>

#include <quaternionFilters.h>

// Positioning Chip 1 - head mounted
MPU9250 headPosition;
//MPU9250 chinPosition;

float headPitch = 0.0;
float headYaw = 0.0;
float headRoll = 0.0;

///////////////////////////////////////////////////////////////////////////////////////
// END : Positioning -----------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////
// START : Audio ---------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////

#include <MIDI.h>
#include "noteList.h"
#include "pitches.h"

#define MIDI_CHANNEL_OUTPUT 1	// MIDI Channel that gets sent out

byte gain = 0;			// volume for output
byte note = 0;			// next / last pitch
byte octave = 1;

// #include <SoftwareSerial.h>
// SoftwareSerial midiSerial( 2,3 );
// MIDI_CREATE_INSTANCE(SoftwareSerial, midiSerial, midiBench);
MIDI_CREATE_DEFAULT_INSTANCE();

// cache
// static const unsigned sMaxNumNotes = 16;
// MidiNoteList<sMaxNumNotes> midiNotes;

///////////////////////////////////////////////////////////////////////////////////////
// END : Audio -----------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////
// START : Lighting ------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////

#include <FastLED.h>

#define NUM_PIXELS      10       // how many neopixels are on the ribbon

CRGB leds[NUM_PIXELS];		// Fast LED objects

byte saturation = 128;		// the intensity of the colour
byte brightness = 255;		// lightness of the LEDs for the NeoPixel Outputs
byte angle = 0;                 // colour angle

///////////////////////////////////////////////////////////////////////////////////////
// END : Lighting --------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////////////


// ===================================================================================


//////////////////////////////////////////////////////////////////////////////////////
// Setup the Flashy Lights =P
//////////////////////////////////////////////////////////////////////////////////////
void setupLights()
{
    FastLED.addLeds<NEOPIXEL, PIN_LED_STRIP_B>(leds, NUM_PIXELS);
}

/////////////////////////////////////lo./////////////////////////////////////////////////
// Setup the Position sensors
//////////////////////////////////////////////////////////////////////////////////////
void setupPositioning()
{
    byte c = headPosition.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);

    // if we are debugging via a computer...
    Serial.print("MPU9250 "); Serial.print("I AM "); Serial.print(c, HEX);
    Serial.print(" I should be "); Serial.println(0x71, HEX);

    // WHO_AM_I should always be 0x68 according to the docs, but not the examples...
    switch (c)
    {
	// case 0x68:
	// default:
	case 0x71:
	{
	    Serial.println("MPU9250 is online...");

	    // Start by performing self test and reporting values
	    headPosition.MPU9250SelfTest(headPosition.SelfTest);
	    Serial.print("x-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[0],1); Serial.println("% of factory value");
	    Serial.print("y-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[1],1); Serial.println("% of factory value");
	    Serial.print("z-axis self test: acceleration trim within : ");
	    Serial.print(headPosition.SelfTest[2],1); Serial.println("% of factory value");
	    Serial.print("x-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[3],1); Serial.println("% of factory value");
	    Serial.print("y-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[4],1); Serial.println("% of factory value");
	    Serial.print("z-axis self test: gyration trim within : ");
	    Serial.print(headPosition.SelfTest[5],1); Serial.println("% of factory value");

	    // Calibrate gyro and accelerometers, load biases in bias registers
	    headPosition.calibrateMPU9250(headPosition.gyroBias, headPosition.accelBias);

	    // Initialize device for active mode read of acclerometer, gyroscope, and
	    // temperature
	    headPosition.initMPU9250();
	    Serial.println("MPU9250 initialized for active data mode....");

	    // Read the WHO_AM_I register of the magnetometer, this is a good test of
	    // communication
	    byte d = headPosition.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
	    Serial.print("AK8963 "); Serial.print("I AM "); Serial.print(d, HEX);
	    Serial.print(" I should be "); Serial.println(0x48, HEX);

	    // Get magnetometer calibration from AK8963 ROM
	    headPosition.initAK8963(headPosition.magCalibration);

	    // Initialize device for active mode read of magnetometer
	    Serial.println("AK8963 initialized for active data mode....");
	    if (SERIAL_DEBUG)
	    {
	    //  Serial.println("Calibration values: ");
		Serial.print("X-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[0], 2);
		Serial.print("Y-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[1], 2);
		Serial.print("Z-Axis sensitivity adjustment value ");
		Serial.println(headPosition.magCalibration[2], 2);
	    }
	}
	break;

	//case 0x68:
	default:
	{
	      Serial.print("Could not connect to MPU9250: 0x");
	      Serial.println(c, HEX);
	      while(1) ; // Loop forever if communication doesn't happen
	}
	break;

    }
}

//////////////////////////////////////////////////////////////////////////////////////
// Setup our capacitance panels to accept touches
//////////////////////////////////////////////////////////////////////////////////////
void setupButtons()
{
    capacitanceBaselineTop = ADCTouch.read( PIN_CAPACITIVE_TOP,  500 );
    capacitanceBaselineBottom = ADCTouch.read( PIN_CAPACITIVE_BOTTOM,  500 );

}

//////////////////////////////////////////////////////////////////////////////////////
// Create our Audio Interface!
//////////////////////////////////////////////////////////////////////////////////////
void setupAudio()
{
    // if we want SYNCH out too...
    // for cheating things mainly ;)
    //pinMode(PIN_GATE_TRIGGER, OUTPUT);
    //pinMode(PIN_SPEAKER, OUTPUT);

    if (!SERIAL_DEBUG)
    {
        //MIDI.setHandleNoteOn(onNoteOn);
        //MIDI.setHandleNoteOff(onNoteOff);p
        // Listen to ALL channels...
        //MIDI.begin( MIDI_CHANNEL_OMNI );

        // Listen to NO channel
        MIDI.begin( MIDI_CHANNEL_OFF );
    }
}

inline void playNote( byte pitch, byte velocity )
{
    // only play if we are not SERIAL_DEBUGging
    if (SERIAL_DEBUG) return;
    // uint16_t pitch = sNotePitches[ noteKey ];
    MIDI.sendNoteOn( pitch, velocity, MIDI_CHANNEL_OUTPUT );   // midiNote.velocity
    digitalWrite(PIN_ARDUINO_LED, HIGH );
}

inline void stopNote( byte pitch )
{
    if (SERIAL_DEBUG) return;
    MIDI.sendNoteOff( pitch, 0, MIDI_CHANNEL_OUTPUT );
    digitalWrite(PIN_ARDUINO_LED, LOW );
}


// Initialisation ====================================================================
void setup()
{
    // Serial.begin(9600); 		// for Teensy 3.1, beware printout can cause glitches
    // Serial.begin(115200);
    Serial.begin(38400);

    if (SERIAL_DEBUG)
    {
      Serial.begin(9600);
      Serial.print("Beard Organ v."); Serial.println("0.5");
    }

    pinMode( PIN_ARDUINO_LED, OUTPUT );

    // Initiate the Wire library and join the I2C bus as a master or slave.
    // This should normally be called only once.
    // http://playground.arduino.cc/Main/WireLibraryDetailedReference
    //Wire.begin();

    // Interface
    if (USE_CAPACTIVE_TOUCH) setupButtons();

    // Lighting
    setupLights();

    // Positioning
    if (USE_POSITIONING) setupPositioning();

    // Audio MIDI Last...
    setupAudio();
}


///////////////////////////////////////////////////////////////////////////////////////
// PROCESS SENSOR DATA
// Process and manage the actuator data from the onboard sensors...
// All data is saved in the global space
///////////////////////////////////////////////////////////////////////////////////////
boolean processSensorData()
{
    boolean hasChanged = false;

    // no change here, don't bother changing hasChanged
    if (USE_CAPACTIVE_TOUCH) processCapacitanceSensor();

    if ( processConductiveRubberSensor() ) hasChanged = true;
    if ( processVolumeDialPotentiometer() ) hasChanged = true;
    if ( USE_POSITIONING && processPositionSensor( headPosition ) )
    {
        headPitch = headPosition.pitch;
        headYaw = headPosition.yaw;
        headRoll = headPosition.roll;
        hasChanged = true;
    }
    return hasChanged;
}

boolean processCapacitanceSensor()
{
    // Analg pin to read, amount of samples to take (defaults to 100)
    int topRingReading = ADCTouch.read( PIN_CAPACITIVE_TOP,  100 );
    int bottomRingReading = ADCTouch.read( PIN_CAPACITIVE_BOTTOM,  100 );

    topRingReading -= capacitanceBaselineTop;
    bottomRingReading -= capacitanceBaselineBottom;

    if (topRingReading > 250)
    {
	// trigger A - set flag...
        if (octave<4) octave++;
    }

    if (bottomRingReading > 250)
    {
	// trigger B - set flag...
        if (octave>0) octave--;
    }

    /*
    if (SERIAL_DEBUG)
    {
        Serial.print("TopRingReading 	: "); Serial.println(topRingReading);
        Serial.print("BottomRingReading : "); Serial.println(bottomRingReading);
    }
    */
}

boolean processConductiveRubberSensor()
{
    // Potential Divider Equation Here :
     float rubberReading = analogRead( PIN_RUBBER_POT );
    /*
    float rubberReading = 0;

    // take N samples in a row, with a slight delay
    for (uint8_t i=0; i< NUMSAMPLES; i++)
    {
        rubberReading += analogRead( PIN_RUBBER_POT );
        // delay(10);
    }

    rubberReading /= NUMSAMPLES;
*/
    // value needs to be 0+ otherwise potential for divide by zero!
    if (rubberReading > 0.0 && rubberReading != lastRubberReading)
    {
	// between 90 and 110...
	// only values above 90 are significant really...
	byte mapped;

    mapped = constrain( rubberReading, lowerLimit, upperLimit );
    mapped = map(rubberReading, lowerLimit, upperLimit, 0, 255);
    mapped = 255 - mapped;

	// use this mapped byte to control something...
	//note = mapped;

    byte octaveShift = octave*8;
    // map it to an 8 bit range for efficient calculations (from 0->1023 to 0->255)
    note = map( mapped, 0, 255, 0, 48);
    note += octaveShift;

	if (SERIAL_DEBUG)
	{
	    Serial.print("Rubber Reading : "); Serial.print(rubberReading);
	    Serial.print(" Mapped : "); Serial.println(mapped);
	}

	// V = R / ( R + FIXED_RESISTOR_IN_OHMS ) * 5V
	// float Vout = ((5.0 / 1023.0 ) * rubberReading);
	// float b = ( 5.0 / Vout ) - 1;
	// float resistance = FIXED_RESISTOR_IN_OHMS / b;

	// rubberReading = (1023 / rubberReading) -1;    // fix range 0 -> 1
	// rubberReading = FIXED_RESISTOR_IN_OHMS / rubberReading;

	//Serial.print(", Resistance : "); Serial.print(rubberReading);
	//Serial.print(", Hi : "); Serial.print(highRubberReading);
	//Serial.print(", Lo : "); Serial.println(lowRubberReading);

	lastRubberReading = rubberReading;
	return true;
    }
    return false;
}

boolean processVolumeDialPotentiometer()
{
    // read the variable resistor for volume
    int volumePotValue = analogRead( PIN_GAIN_POT );	// value is 0-1023

    // map it to an 8 bit range for efficient calculations (from 0->1023 to 0->255)
    // due to reasons beyond my understanding, the POT has a bottom limit of 120...
    // this seems to be due to some shonky wiring
    byte volume = map(volumePotValue, 120, 1023, 0, 127);

    // int notePotValue = analogRead( PIN_NOTE_POT );	// value is 0-1023
    // check for change...

    // Serial.println(volumePotValue);
    // Serial.println("POT:"); Serial.print(volumePotValue); Serial.print(" VOL:"); Serial.print(volume); Serial.print("\n");


    // check to see if it has changed and if so update other stuff...
    if ( gain != volume )
    {

        if (SERIAL_DEBUG)
        {
            Serial.print("Volume changed..POT:"); Serial.print(volumePotValue); Serial.print(" VOL:"); Serial.println(volume);
        }
        // save updated values
        brightness = volume;
        gain = volume;
	   return true;

    }else{

        //Serial.println("Volume unchanged."); Serial.print(gain); Serial.print( " == " );Serial.print(volume); Serial.print("\n");
        return false;
    }
}

//////////////////////////////////////////////////////////////////////////////////
// Read the positions
//////////////////////////////////////////////////////////////////////////////////
boolean processPositionSensor( MPU9250 positionSensor )
{
    // If intPin goes high, all data registers have new data
    // On interrupt, check if data ready interrupt
    if (positionSensor.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)
    {
        positionSensor.readAccelData(positionSensor.accelCount);  // Read the x/y/z adc values
        positionSensor.getAres();

        // Now we'll calculate the accleration value into actual g's
        // This depends on scale being set
        positionSensor.ax = (float)positionSensor.accelCount[0]*positionSensor.aRes; // - accelBias[0];
        positionSensor.ay = (float)positionSensor.accelCount[1]*positionSensor.aRes; // - accelBias[1];
        positionSensor.az = (float)positionSensor.accelCount[2]*positionSensor.aRes; // - accelBias[2];

        positionSensor.readGyroData(positionSensor.gyroCount);  // Read the x/y/z adc values
        positionSensor.getGres();

        // Calculate the gyro value into actual degrees per second
        // This depends on scale being set
        positionSensor.gx = (float)positionSensor.gyroCount[0]*positionSensor.gRes;
        positionSensor.gy = (float)positionSensor.gyroCount[1]*positionSensor.gRes;
        positionSensor.gz = (float)positionSensor.gyroCount[2]*positionSensor.gRes;

        positionSensor.readMagData(positionSensor.magCount);  // Read the x/y/z adc values
        positionSensor.getMres();
        // User environmental x-axis correction in milliGauss, should be
        // automatically calculated
        positionSensor.magbias[0] = +470.;
        // User environmental x-axis correction in milliGauss TODO axis??
        positionSensor.magbias[1] = +120.;
        // User environmental x-axis correction in milliGauss
        positionSensor.magbias[2] = +125.;

        // Calculate the magnetometer values in milliGauss
        // Include factory calibration per data sheet and user environmental
        // corrections
        // Get actual magnetometer value, this depends on scale being set
        positionSensor.mx = (float)positionSensor.magCount[0]*positionSensor.mRes*positionSensor.magCalibration[0] -
                   positionSensor.magbias[0];
        positionSensor.my = (float)positionSensor.magCount[1]*positionSensor.mRes*positionSensor.magCalibration[1] -
                   positionSensor.magbias[1];
        positionSensor.mz = (float)positionSensor.magCount[2]*positionSensor.mRes*positionSensor.magCalibration[2] -
                   positionSensor.magbias[2];
    }

    // Must be called before updating quaternions!
    positionSensor.updateTime();

    // Sensors x (y)-axis of the accelerometer is aligned with the y (x)-axis of
    // the magnetometer; the magnetometer z-axis (+ down) is opposite to z-axis
    // (+ up) of accelerometer and gyro! We have to make some allowance for this
    // orientationmismatch in feeding the output to the quaternion filter. For the
    // MPU-9250, we have chosen a magnetic rotation that keeps the sensor forward
    // along the x-axis just like in the LSM9DS0 sensor. This rotation can be
    // modified to allow any convenient orientation convention. This is ok by
    // aircraft orientation standards! Pass gyro rate as rad/s
    //  MadgwickQuaternionUpdate(ax, ay, az, gx*PI/180.0f, gy*PI/180.0f, gz*PI/180.0f,  my,  mx, mz);
    MahonyQuaternionUpdate(positionSensor.ax, positionSensor.ay, positionSensor.az, positionSensor.gx*DEG_TO_RAD,
                         positionSensor.gy*DEG_TO_RAD, positionSensor.gz*DEG_TO_RAD, positionSensor.my,
                         positionSensor.mx, positionSensor.mz, positionSensor.deltat);

    if (!AHRS)
    {
        positionSensor.delt_t = millis() - positionSensor.count;
        if (positionSensor.delt_t > 500)
        {
            if(SERIAL_DEBUG)
            {
                // Print acceleration values in milligs!
                Serial.print("X-acceleration: "); Serial.print(1000*positionSensor.ax);
                Serial.print(" mg ");
                Serial.print("Y-acceleration: "); Serial.print(1000*positionSensor.ay);
                Serial.print(" mg ");
                Serial.print("Z-acceleration: "); Serial.print(1000*positionSensor.az);
                Serial.println(" mg ");

                // Print gyro values in degree/sec
                Serial.print("X-gyro rate: "); Serial.print(positionSensor.gx, 3);
                Serial.print(" degrees/sec ");
                Serial.print("Y-gyro rate: "); Serial.print(positionSensor.gy, 3);
                Serial.print(" degrees/sec ");
                Serial.print("Z-gyro rate: "); Serial.print(positionSensor.gz, 3);
                Serial.println(" degrees/sec");

                // Print mag values in degree/sec
                Serial.print("X-mag field: "); Serial.print(positionSensor.mx);
                Serial.print(" mG ");
                Serial.print("Y-mag field: "); Serial.print(positionSensor.my);
                Serial.print(" mG ");
                Serial.print("Z-mag field: "); Serial.print(positionSensor.mz);
                Serial.println(" mG");

                positionSensor.tempCount = positionSensor.readTempData();  // Read the adc values
                // Temperature in degrees Centigrade
                positionSensor.temperature = ((float) positionSensor.tempCount) / 333.87 + 21.0;
                // Print temperature in degrees Centigrade
                Serial.print("Temperature is ");  Serial.print(positionSensor.temperature, 1);
                Serial.println(" degrees C");
            }
            positionSensor.count = millis();

            // toggle led?
            //digitalWrite(ARDUINO_LED_PIN, !digitalRead(ARDUINO_LED_PIN));
        }

    } else {

        // Serial print and/or display at 0.5 s rate independent of data rates
        positionSensor.delt_t = millis() - positionSensor.count;

        // update LCD once per half-second independent of read rate
        if (positionSensor.delt_t > 500)
        {
            if(SERIAL_DEBUG)
            {
                Serial.print("ax = "); Serial.print((int)1000*positionSensor.ax);
                Serial.print(" ay = "); Serial.print((int)1000*positionSensor.ay);
                Serial.print(" az = "); Serial.print((int)1000*positionSensor.az);
                Serial.println(" mg");

                Serial.print("gx = "); Serial.print( positionSensor.gx, 2);
                Serial.print(" gy = "); Serial.print( positionSensor.gy, 2);
                Serial.print(" gz = "); Serial.print( positionSensor.gz, 2);
                Serial.println(" deg/s");

                Serial.print("mx = "); Serial.print( (int)positionSensor.mx );
                Serial.print(" my = "); Serial.print( (int)positionSensor.my );
                Serial.print(" mz = "); Serial.print( (int)positionSensor.mz );
                Serial.println(" mG");

		// This relies on Quarternions.h

                // Serial.print("q0 = "); Serial.print(*getQ());
                // Serial.print(" qx = "); Serial.print(*(getQ() + 1));
                // Serial.print(" qy = "); Serial.print(*(getQ() + 2));
                // Serial.print(" qz = "); Serial.println(*(getQ() + 3));
            }

            // Define output variables from updated quaternion---these are Tait-Bryan
            // angles, commonly used in aircraft orientation. In this coordinate system,
            // the positive z-axis is down toward Earth. Yaw is the angle between Sensor
            // x-axis and Earth magnetic North (or true North if corrected for local
            // declination, looking down on the sensor positive yaw is counterclockwise.
            // Pitch is angle between sensor x-axis and Earth ground plane, toward the
            // Earth is positive, up toward the sky is negative. Roll is angle between
            // sensor y-axis and Earth ground plane, y-axis up is positive roll. These
            // arise from the definition of the homogeneous rotation matrix constructed
            // from quaternions. Tait-Bryan angles as well as Euler angles are
            // non-commutative; that is, the get the correct orientation the rotations
            // must be applied in the correct order which for this configuration is yaw,
            // pitch, and then roll.
            // For more see
            // http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
            // which has additional links.

            // Needs Quaternions.h
            positionSensor.yaw   = atan2(2.0f * (*(getQ()+1) * *(getQ()+2) + *getQ() *
            *(getQ()+3)), *getQ() * *getQ() + *(getQ()+1) * *(getQ()+1)
            - *(getQ()+2) * *(getQ()+2) - *(getQ()+3) * *(getQ()+3));

            positionSensor.pitch = -asin(2.0f * (*(getQ()+1) * *(getQ()+3) - *getQ() *
            *(getQ()+2)));

            positionSensor.roll  = atan2(2.0f * (*getQ() * *(getQ()+1) + *(getQ()+2) *
            *(getQ()+3)), *getQ() * *getQ() - *(getQ()+1) * *(getQ()+1)
            - *(getQ()+2) * *(getQ()+2) + *(getQ()+3) * *(getQ()+3));


            positionSensor.pitch *= RAD_TO_DEG;
            positionSensor.yaw   *= RAD_TO_DEG;

            // Declination of SparkFun Electronics (40°05'26.6"N 105°11'05.9"W) is
            // 	8° 30' E  ± 0° 21' (or 8.5°) on 2016-07-19
            // - http://www.ngdc.noaa.gov/geomag-web/#declination
            // Royal Festival Hall is at
            //
            positionSensor.yaw   -= 8.5;
            positionSensor.roll  *= RAD_TO_DEG;

            if(SERIAL_DEBUG)
            {
                Serial.print("Yaw, Pitch, Roll: ");
                Serial.print(positionSensor.yaw, 2);
                Serial.print(", ");
                Serial.print(positionSensor.pitch, 2);
                Serial.print(", ");
                Serial.println(positionSensor.roll, 2);

                Serial.print("rate = ");
                Serial.print((float)positionSensor.sumCount/positionSensor.sum, 2);
                Serial.println(" Hz");
            }

            positionSensor.count = millis();
            positionSensor.sumCount = 0;
            positionSensor.sum = 0;
        }
    }


    return true; // change has occurred...
}


//////////////////////////////////////////////////////////////////////////////////
// Loop de loop
//////////////////////////////////////////////////////////////////////////////////

long timePreviousNoteWasPlayed = 0;


void loop()
{
    boolean updateNeccessary = false;

    // Watch for MIDI signals...
    if ( MIDI.read() )
    {
        // MIDI Message!
        updateNeccessary = true;

    }else{

        // No MIDI interupt, so let's fetch the sensor data
        updateNeccessary = processSensorData();
    }

    /*
    //Serial.println("updateControl gain: ");
    //Serial.print(gain);

    // put changing controls in here
    if (counter++ >= THROTTLE)
    {
        // force update
        // updateNeccessary = true;
        // reset counter
        counter %= THROTTLE;
    }
    */

    if (true)
    {
        //Serial.println("-------------------------------"); Serial.print("updating...");

        // set the angle to the note...
        // angle = note;

        // Control the LEDS
        setPixels();

        // Send out some audio
       // playNote( note, gain / 2 );

        if (!SERIAL_DEBUG)
        {
             manageNoteEvents();
            if(timePreviousNoteWasPlayed + 500 < millis())
            {
               timePreviousNoteWasPlayed = millis();
               //noteDuration += 100;
               addNoteEvent( note, gain, noteDuration );
            }
            //Serial.println("MIDI.sendNoteOn:"); Serial.print(note); Serial.print(" CHANNEL:"); Serial.print(MIDI_CHANNEL_OUTPUT); Serial.print("\n");
        }


	// Not neccessary - just for testing...
	// Wait a little while otherwise we are going to flood all the sensors!
//	delay(100);
    }else{

        // change colours if waiting...
        angle++;

        // update lamps
        setPixels();

        // Wait a little while otherwise we are going to flood all the sensors!
        //
    }

    delay(10);
    //Serial.println("Counter:"); Serial.print( counter ); Serial.print("\n");
}


// Internal Methods ============================================================

//////////////////////////////////////////////////////////////////////////////////
// Control the LED pixels and beard lights!
//////////////////////////////////////////////////////////////////////////////////
void setPixels()
{
    // Clear existing values...
    FastLED.clear();

    // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
    for( int i=0; i<NUM_PIXELS; i++ )
    {
        // Hue Saturation Brightness !
        // fetch previous values ...
        // byte hue = random(255);
        leds[i] = CHSV( angle, saturation, brightness );

        // Serial.println("==================");
        // Serial.println("Painting LED : ");
        // Serial.println(i);
        // Serial.println("With RGB > ");
        // Serial.print( " R:" ); Serial.print( leds[i].red );
        // Serial.print( " G:" ); Serial.print( leds[i].green );
        // Serial.print( " B:" ); Serial.print( leds[i].blue );
        // Serial.println( " H:" ); Serial.print( hue );
        // Serial.print( " S:187" );
        // Serial.print( " V:" ); Serial.print( brightness );
        // Serial.println("==================");

        // or via RGB
        //leds[i].r = 255;
        //leds[i].g = 68;
        //leds[i].b = 221;
    }

    // This sends the updated pixel color to the hardware.
    FastLED.show();

    /*
    if (SERIAL_DEBUG)
    {
	Serial.println("==================");
	Serial.print( " R:" ); Serial.print( leds[0].red );
	Serial.print( " G:" ); Serial.print( leds[0].green );
	Serial.print( " B:" ); Serial.print( leds[0].blue );
	Serial.print( "  /  " );
	Serial.print( " H:" ); Serial.print( angle );
	Serial.print( " S:" ); Serial.print( saturation );
	Serial.print( " V:" ); Serial.print( brightness );
	Serial.println("==================");
    }
    */
}




// For enabling MIDI In as well as out...
// You can then use it like a MIDI through, but with the extra data superimposed
// -----------------------------------------------------------------------------
/*
inline void handleGateChanged(bool inGateActive)
{
    digitalWrite(sGatePin, inGateActive ? HIGH : LOW);
}

inline void pulseGate()
{
// show onboard light!
    digitalWrite(PIN_ARDUINO_LED, HIGH );
    handleGateChanged(false);
    delay(1);
    handleGateChanged(true);
    digitalWrite(PIN_ARDUINO_LED, LOW );
}


void handleNotesChanged(bool isFirstNote = false)
{
    if (midiNotes.empty())
    {
        handleGateChanged(false);
        noTone(PIN_SPEAKER);
    }
    else
    {
        // Possible playing modes:
        // Mono Low:  use midiNotes.getLow
        // Mono High: use midiNotes.getHigh
        // Mono Last: use midiNotes.getLast

        byte currentNote = 0;
        if (midiNotes.getLast(currentNote))
        {
            tone(PIN_SPEAKER, sNotePitches[currentNote]);

            if (isFirstNote)
            {
                handleGateChanged(true);
            }
            else
            {
                pulseGate(); // Retrigger envelopes. Remove for legato effect.
            }
        }
    }
}

// External comms
void onNoteOn(byte inChannel, byte inNote, byte inVelocity)
{
    const bool firstNote = midiNotes.empty();
    midiNotes.add(MidiNote(inNote, inVelocity));
    handleNotesChanged(firstNote);
}

void onNoteOff(byte inChannel, byte inNote, byte inVelocity)
{
    midiNotes.remove(inNote);
    handleNotesChanged();
}
*/

