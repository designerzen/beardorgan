struct NoteEvent {
   int endTime; 
   byte pitch; 
   bool on;
};

NoteEvent theNoteEvent = NoteEvent{ 0, 0, false };

void addNoteEvent( byte pitch, byte velocity, int duration )
{
  //Check if there has been a note event 
  if( theNoteEvent.on )
  {
     stopNote( theNoteEvent.pitch ); 
      //return;
  }

  // Create the new note 
  theNoteEvent.pitch = pitch; 
  theNoteEvent.endTime = millis() + duration; 
  theNoteEvent.on = true; 

  playNote( pitch, velocity ); 
}

void manageNoteEvents()
{
  if( theNoteEvent.on && theNoteEvent.endTime < millis() )
  {
       stopNote( theNoteEvent.pitch ); 
       theNoteEvent.on = false;
  }
}

